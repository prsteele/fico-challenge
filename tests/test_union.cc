#include <gtest/gtest.h>

#include "union.hh"

TEST(UnionTest, TestIsSanctionedMinute) {
  auto is_not_sanctioned_minute = [] (long z) {
    return !Union::is_sanctioned_minute(z);
  };
  
  for (long d = 0; d < 3; d++) {
    for (long i = 0; i < Union::START_OFFSET; i++) {
      auto z = d * Union::DAY_DURATION + i;
      ASSERT_PRED1(is_not_sanctioned_minute, z);
    }
    for (long i = Union::START_OFFSET; i < Union::END_OFFSET; i++) {
      auto z = d * Union::DAY_DURATION + i;
      ASSERT_PRED1(Union::is_sanctioned_minute, z);
    }
    for (long i = Union::END_OFFSET; i < Union::DAY_DURATION; i++) {
      auto z = d * Union::DAY_DURATION + i;
      ASSERT_PRED1(is_not_sanctioned_minute, z);
    }
  }
}

TEST(UnionTest, TestSanctionedTimeDuring) {
  auto pred = [] (const char* a_name, const char* b_name, const char* exp_name,
                  long a, long b, long exp) {
    auto computed = Union::sanctioned_time_during(a, b);

    if (computed == exp) {
      return testing::AssertionSuccess();
    } else {
      return testing::AssertionFailure()
      << "Union::sanctioned_time_during("
      << a << ", " << b << ") is " << computed << ", not " << exp;
    }
  };
  
  // Test all pairs of times with maximum value 3 days.
  for (long a = 0; a <= 3 * Union::DAY_DURATION; a++) {
    long sanctioned = 0;
    for (long b = a; b <= 3 * Union::DAY_DURATION; b++) {
      if (b > a and Union::is_sanctioned_minute(b - 1)) {
        sanctioned++;
      }
      
      ASSERT_PRED_FORMAT3(pred, a, b, sanctioned);
    }
  }
}

TEST(UnionTest, TestStartWorkPeriod) {
  auto pred = [] (const char* t_name, const char* r_name, const char* exp_name,
                  long t, long r, long exp) {
    auto computed = Union::start_work_period(t, r);

    if (computed == exp) {
      return testing::AssertionSuccess();
    } else {
      return testing::AssertionFailure()
      << "Union::start_work_period("
      << t << ", " << r << ") is " << computed << ", not " << exp;
    }
  };

  // Start time is before sanctioned time
  for (long t = 0; t <= Union::START_OFFSET; t++) {
    for (long r = 0; r < Union::SANCTIONED_DURATION; r++) {

      if (r == 0) {
        ASSERT_PRED_FORMAT3(pred, t, r, t);
      } else {
        ASSERT_PRED_FORMAT3(pred, t, r, Union::START_OFFSET + r);
      }
    }
  }

  // Start time is during sanctioned time
  for (long t = Union::START_OFFSET; t <= Union::END_OFFSET; t++) {
    for (long r = 0; r < Union::SANCTIONED_DURATION; r++) {

      auto missing = t - Union::START_OFFSET;

      if (r == 0) {
        if (t == Union::END_OFFSET) {
          ASSERT_PRED_FORMAT3(pred, t, r, Union::DAY_DURATION + Union::START_OFFSET);
        } else {
          ASSERT_PRED_FORMAT3(pred, t, r, t);
        }
      } else if (t + r <= Union::END_OFFSET) {
        ASSERT_PRED_FORMAT3(pred, t, r, t + r);
      } else {
        auto day = t / Union::DAY_DURATION;
        auto next_start = (day + 1) * Union::DAY_DURATION + Union::START_OFFSET;
        auto remaining = r - (Union::SANCTIONED_DURATION - missing);
        ASSERT_PRED_FORMAT3(pred, t, r, next_start + remaining);
      }
    }
  }

  // Start time is after sanctioned time
  for (long t = Union::END_OFFSET + 1; t < Union::DAY_DURATION; t++) {
    for (long r = 0; r < Union::SANCTIONED_DURATION; r++) {

      if (r == 0) {
        ASSERT_PRED_FORMAT3(pred, t, r, t);
      } else {
        auto day = t / Union::DAY_DURATION;
        auto next_start = (day + 1) * Union::DAY_DURATION + Union::START_OFFSET;
        ASSERT_PRED_FORMAT3(pred, t, r, next_start + r);
      }
    }
  }

  // Miscellaneous tests for rest periods spanning multiple days
  ASSERT_PRED_FORMAT3(pred, 0, Union::SANCTIONED_DURATION * 2,
                      Union::DAY_DURATION * 2 + Union::START_OFFSET);

  ASSERT_PRED_FORMAT3(pred, 0, Union::SANCTIONED_DURATION * 3 + 1,
                      Union::DAY_DURATION * 3 + Union::START_OFFSET + 1);

  ASSERT_PRED_FORMAT3(pred, Union::DAY_DURATION * 2 + Union::END_OFFSET, 1,
                      Union::DAY_DURATION * 3 + Union::START_OFFSET + 1);
}

TEST(UnionTest, TestNextSanctionedMinute) {
   auto pred = [] (const char* t_name, const char* exp_name, long t, long exp) {
     auto computed = Union::next_sanctioned_minute(t);

     if (computed == exp) {
       return testing::AssertionSuccess();
     } else {
       return testing::AssertionFailure()
       << "Union::next_sanctioned_minute("
       << t << ") is " << computed << ", not " << exp;
     }
   };

   for (long t = 0; t < 3 * Union::DAY_DURATION; t++) {
     auto day_start = (t / Union::DAY_DURATION) * Union::DAY_DURATION;
     auto offset = t % Union::DAY_DURATION;

     if (offset < Union::START_OFFSET) {
       ASSERT_PRED_FORMAT2(pred, t, day_start + Union::START_OFFSET);
     } else if (offset < Union::END_OFFSET - 1) {
       ASSERT_PRED_FORMAT2(pred, t, t + 1);
     } else {
       ASSERT_PRED_FORMAT2(pred, t, day_start + Union::DAY_DURATION + Union::START_OFFSET);
     }
   }

   ASSERT_PRED_FORMAT2(pred, 1439, 1980);
}

# FICO Challenge

This directory contains code related to the Kaggle.com challenge
"Helping Santa's Helpers", a large-scale scheduling problem.

## FICO

We use the FICO solver for solving some sub-problems.

To use FICO, we need to source the
`<path-to-xpress>/xpressmp/bin/xpvars.sh` file.

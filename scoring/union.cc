#include "union.hh"

#include <iostream>

auto Union::is_sanctioned_minute(long t) -> bool
{
  auto offset = t % Union::DAY_DURATION;

  return Union::START_OFFSET <= offset and offset < Union::END_OFFSET;
}

auto Union::sanctioned_time_during(long a, long b) -> long
{
  auto first_day = a / Union::DAY_DURATION;
  auto first_minute = a % Union::DAY_DURATION;
  auto last_day = b / Union::DAY_DURATION;
  auto last_minute = b % DAY_DURATION;

  auto end_first_sanctioned =
    std::min(first_day * Union::DAY_DURATION + Union::END_OFFSET, b);

  
  auto first_sanctioned =
    std::max(0L, end_first_sanctioned % Union::DAY_DURATION
             - std::max(Union::START_OFFSET, first_minute));

  auto inter_sanctioned =
    std::max(0L, (last_day - first_day - 1) * Union::SANCTIONED_DURATION);

  
  if (last_day > first_day) {
    auto last_sanctioned =
      std::max(0L, std::min(last_minute, Union::END_OFFSET)
               - Union::START_OFFSET);
  
    return first_sanctioned + inter_sanctioned + last_sanctioned;
  } else {
    return first_sanctioned + inter_sanctioned;
  }
}

auto Union::unsanctioned_time_during(long a, long b) -> long
{
  return (b - a) - Union::sanctioned_time_during(a, b);
}

auto Union::next_sanctioned_minute(long t) -> long
{
  auto tt = t + 1;
  
  if (Union::is_sanctioned_minute(tt)) {
    return tt;
  } else {
    return Union::next_sanctioned_start(tt);
  }
}

auto Union::next_sanctioned_start(long t) -> long
{
  // Sanctioned start times are of the form
  //
  //   9 * 60 + n * 24 * 60
  //
  // for any nonnegative integer n.

  auto minute_of_day = t % Union::DAY_DURATION;
  if (minute_of_day < Union::START_OFFSET) {
    return t + Union::START_OFFSET - minute_of_day;
  }

  auto day = Union::get_day(t);

  return (day + 1) * Union::DAY_DURATION + Union::START_OFFSET;
}

auto Union::next_sanctioned_end(long t) -> long
{
  // Sanctioned start times are of the form
  //
  //   19 * 60 + n * 24 * 60
  //
  // for any nonnegative integer n.

  auto minute_of_day = t % Union::DAY_DURATION;
  if (minute_of_day < Union::END_OFFSET) {
    return t + Union::END_OFFSET - minute_of_day;
  }

  auto day = Union::get_day(t);

  return (day + 1) * Union::DAY_DURATION + Union::END_OFFSET;
}

auto Union::get_day(long t) -> long
{
  // t >= 24 * 60 * n
  return t / Union::DAY_DURATION;
}

auto Union::start_work_period(long t, long r) -> long
{
  if (r == 0) {
    if (Union::is_last_sanctioned_minute(t)) {
      return Union::next_sanctioned_minute(t);
    } else {
      return t;
    }
  }

  auto day = t / Union::DAY_DURATION;
  auto rest_days = r / Union::SANCTIONED_DURATION;
  auto rest_minutes = r % Union::SANCTIONED_DURATION;

  auto offset = t % Union::DAY_DURATION;
  
  if (offset < Union::START_OFFSET) {
    offset = Union::START_OFFSET;
  } else if (offset > Union::END_OFFSET) {
    day++;
    offset = Union::START_OFFSET;
  }

  if (offset + rest_minutes > Union::END_OFFSET) {
    rest_days++;
    rest_minutes -= Union::END_OFFSET - offset;
    offset = Union::START_OFFSET;
  }

  auto z = (day + rest_days) * Union::DAY_DURATION + offset + rest_minutes;
  return z;
  if (Union::is_last_sanctioned_minute(z)) {
    return Union::next_sanctioned_minute(z);
  } else {
    return z;
  }
}

auto Union::is_last_sanctioned_minute(long t) -> bool
{
  return (t % Union::DAY_DURATION) == Union::END_OFFSET;
}

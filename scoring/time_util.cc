#include "time_util.hh"

auto time_util::get_minutes_from_date(std::string date_string) -> long
{
  // Process the date. We must break it into YYYY-MM-DD, then hours and minutes.
  int end_year = date_string.find_first_of(' ');
  int end_month = date_string.find_first_of(' ', end_year + 1);
  int end_day = date_string.find_first_of(' ', end_month + 1);
  int end_hour = date_string.find_first_of(' ', end_day + 1);
  int end_minute = date_string.find_first_of(' ', end_hour + 1);

  int year = std::stol(date_string.substr(0, end_year));
  int month = std::stol(date_string.substr(end_year + 1, end_month - end_year));
  int day = std::stol(date_string.substr(end_month + 1, end_day - end_month));
  int hour = std::stol(date_string.substr(end_day + 1, end_hour - end_day));
  int minute = std::stol(date_string.substr(end_hour + 1, end_minute - end_hour));

  boost::gregorian::date date (year, month, day);

  long days_since_epoch = (date - time_util::epoch).days();

  return days_since_epoch * Union::DAY_DURATION
    + hour * Union::HOUR_DURATION + minute;
}

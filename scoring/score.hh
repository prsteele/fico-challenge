#ifndef SCORE_HH
#define SCORE_HH

#include <string>

class Score
{
public:
  /**
   * Construct a new Score for a feasible solution.
   */
  Score(double objective_value);

  /**
   * Construct a new Score for an infeasible solution.
   */
  Score(std::string message);
  
  /// Was the solution feasible?
  bool feasible;

  /// The objective value of the solution.
  double objective_value;

  /// A string describing the feasibility of the solution
  std::string message;
};

#endif

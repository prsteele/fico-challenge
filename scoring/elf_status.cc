#include "elf_status.hh"

ElfStatus::ElfStatus(Elf::ID id, long availability, double efficiency,
                     long started_at, long stopped_at)
  : id(id),
    availability(availability),
    efficiency(efficiency),
    started_at(started_at),
    stopped_at(stopped_at)
{
}

ElfStatus::ElfStatus(Elf::ID id)
  : id(id),
    availability(Union::START_OFFSET),
    efficiency(1),
    started_at(0),
    stopped_at(0)
{
}

auto ElfStatus::efficiency_after(double eff, double sanc, double unsanc) -> double
{
  auto z = eff
    * pow(Elf::SANCTIONED_RATE, ((double) sanc) / 60.0)
    * pow(Elf::UNSANCTIONED_RATE, ((double) unsanc) / 60.0);

  return std::max(Elf::MIN_EFFICIENCY, std::min(Elf::MAX_EFFICIENCY, z));
}

auto ElfStatus::status_after(long start, std::shared_ptr<Toy> toy)
  -> ElfStatus
{
  auto processing_time = toy->time_at(this->efficiency);

  auto end = start + processing_time;
  auto sanctioned_time = Union::sanctioned_time_during(start, end);
  auto unsanctioned_time = Union::unsanctioned_time_during(start, end);

  auto new_efficiency = ElfStatus::efficiency_after(this->efficiency,
                                              sanctioned_time,
                                              unsanctioned_time);

  auto availability = Union::start_work_period(end, unsanctioned_time);

  ElfStatus ret (this->id, availability, new_efficiency, start, end);
  return ret;
}

auto ElfStatus::status_after(std::shared_ptr<Toy> toy) -> ElfStatus
{
  auto start = std::max(toy->arrival_time, this->availability);
  return ElfStatus::status_after(start, toy);
}

auto ElfStatus::availability_lt(ElfStatus a, ElfStatus b) -> bool
{
  return a.availability < b.availability;
}

auto ElfStatus::availability_gt(ElfStatus a, ElfStatus b) -> bool
{
  return a.availability > b.availability;
}

#ifndef TOY_HH
#define TOY_HH

#include <fstream>
#include <memory>
#include <vector>

#include "time_util.hh"
#include "elf.hh"

// Forward declaration for the following typedef
class Toy;

/// A convenience typedef.
typedef std::vector<std::shared_ptr<Toy>> Toys;

/**
 * A single toy to be built.
 */
class Toy
{
public:
  Toy(long id, long arrival_time, long processing_time);

  /// The ID of this Toy.
  const long id;

  /**
   * Return the processing time for this toy at the given efficiency.
   */
  auto time_at(double efficiency) -> long;
  
  /// The time the Toy arrives at the workshop, in minutes since the
  /// epoch.
  const long arrival_time;

  /// The base time required to create the Toy, in minutes
  const long processing_time;

  auto static load_toys(std::ifstream &file)
    -> Toys;

  static auto release_date_gt(std::shared_ptr<Toy> a, std::shared_ptr<Toy> b) -> bool;
  
  static auto release_date_lt(std::shared_ptr<Toy> a, std::shared_ptr<Toy> b) -> bool;

  static auto processing_time_gt(std::shared_ptr<Toy> a, std::shared_ptr<Toy> b) -> bool;
  
  static auto processing_time_lt(std::shared_ptr<Toy> a, std::shared_ptr<Toy> b) -> bool;
};



#endif

#include "schedule.hh"

Schedule::Schedule(std::ifstream &file,
                   Toys &toys)
  : feasible(true)
{
  std::string line;
  bool once = true;

  while (std::getline(file, line)) {
    // Skip the first line in the file
    if (once) {
      once = false;
      continue;
    }

    int end_toy_id = line.find_first_of(',');
    int end_elf_id = line.find_first_of(',', end_toy_id + 1);
    int end_start_time = line.find_first_of(',', end_elf_id + 1);

    long toy_id = std::stol(line.substr(0, end_toy_id));
    long elf_id = std::stol(line.substr(end_toy_id + 1,
                                        end_elf_id - end_toy_id));
    std::string start_time_string = line.substr(end_elf_id + 1,
                                                end_start_time - end_elf_id);
    long start_time = time_util::get_minutes_from_date(start_time_string);
    long duration = std::stol(line.substr(end_start_time + 1, std::string::npos));

    // If we haven't encountered this elf, add him to the elves list
    // and give him an empty set of instructions
    if (this->instructions.count(elf_id) == 0) {
      this->elves.push_back(elf_id);
      std::vector<Instruction> instrs;
      this->instructions.emplace(elf_id, instrs);

      this->statuses.emplace(elf_id, elf_id);
    }

    auto toy = toys.at(toy_id - 1);

    try {
      this->add_job(elf_id, toy, start_time);
    } catch (std::string &ex) {
      this->feasible = false;
      this->message = ex;
      break;
    }

    auto status = this->statuses.at(elf_id);
    
    if (duration != status.stopped_at - status.started_at) {
      std::ostringstream msg;
      msg << "Bad duration value." << std::endl
          << line << std::endl
          << "Duration should be " << (status.stopped_at - status.started_at);
      this->message = msg.str();
      this->feasible = false;
      break;
    }
  }

  std::sort(this->elves.begin(), this->elves.end());
}

Schedule::Schedule(long number_elves)
  : feasible(true)
{
  for (long i = 1; i <= number_elves; i++) {
    this->elves.push_back(i);
    
    std::vector<Instruction> instrs;
    this->instructions.emplace(i, instrs);

    this->statuses.emplace(i, i);
  }
}

Schedule::Schedule()
  : feasible(true)
{
}

Schedule::Schedule(std::vector<Elf::ID> elves)
  : elves(elves),
    feasible(true)
{
  for (auto &elf : this->elves) {
    this->statuses.emplace(elf, elf);

    std::vector<Instruction> instrs;
    this->instructions.insert(std::make_pair(elf, instrs));
  }
}

auto Schedule::merge(Schedule const& a) -> void
{
  for (auto &kv : a.instructions) {
    this->instructions.at(kv.first) = kv.second;
  }

  for (auto &kv : a.statuses) {
    this->statuses.at(kv.first) = kv.second;
  }
}

auto Schedule::write_solution(std::ostream &file) -> void
{
  // We keep the next instruction for each elf in sorted order by the
  // time it begins.
  auto instruction_comp = [](Instruction a, Instruction b) {
    return a.time < b.time;
  };
  std::priority_queue<Instruction, std::vector<Instruction>,
                      decltype(instruction_comp)>
    instructions (instruction_comp);

  std::map<Elf::ID, decltype(this->instructions.at(0).begin())> start_iterators;
  std::map<Elf::ID, decltype(this->instructions.at(0).end())> end_iterators;

  // Get iterators to the start of each list of instructions
  for (auto &kv : this->instructions) {
    auto start = kv.second.begin();
    auto end = kv.second.end();

    if (start != end) {
      start_iterators.emplace(kv.first, start);
      end_iterators.emplace(kv.first, end);
    } 
  }

  // Populate the initial set of instructions
  for (auto elf : this->elves) {
    auto &it = start_iterators.at(elf);
    instructions.push(*it);

    // Advance the iterators, and remove them if they are exhausted
    it++;
    if (it == end_iterators.at(elf)) {
      start_iterators.erase(elf);
      end_iterators.erase(elf);
    }
  }

  std::ostringstream buffer;
  buffer << "ToyId,ElfId,StartTime,Duration" << std::endl;
  while (!instructions.empty()) {
    auto instr = instructions.top();
    instructions.pop();
    buffer << instr << std::endl;

    // Add another instruction for this elf, if any
    if (start_iterators.count(instr.elf)) {
      auto &it = start_iterators.at(instr.elf);
      instructions.push(*it);

      // Remove the iterators if they are exhausted
      if (it == end_iterators.at(instr.elf)) {
        start_iterators.erase(elf);
        end_iterators.erase(elf);      
      }
    }
  }
  file << buffer.str();
}

auto Schedule::add_job(Elf::ID elf_id, std::shared_ptr<Toy> toy, long time)
  -> void
{
  auto status = this->statuses.at(elf_id);

  if (time < status.availability) {
    std::ostringstream msg;
    msg << "Elf " << elf_id << " can't start work until "
        << status.availability << " but work was requested "
        << "to begin at " << time;

    throw msg.str();
  }

  if (time < toy->arrival_time) {
    std::ostringstream msg;
    msg << "Toy " << toy << " isn't released until "
        << toy->arrival_time << " but work was requested "
        << "to begin at " << time;

    throw msg.str();
  }
  
  auto new_status = status.status_after(time, toy);
  auto duration = new_status.stopped_at - time;
  
  this->instructions.at(elf_id).emplace_back(time, elf_id, toy, duration);
  this->statuses.at(elf_id) = new_status;
}

auto Schedule::add_job(Elf::ID elf_id, std::shared_ptr<Toy> toy) -> void
{
  auto status = this->statuses.at(elf_id);
  auto new_status = status.status_after(toy);
  auto start_time = new_status.started_at;

  this->add_job(elf_id, toy, start_time);
}

auto Schedule::add_job_wait_for_sanctioned(Elf::ID elf_id,
                                           std::shared_ptr<Toy> toy)
  -> void
{
  auto status = this->statuses.at(elf_id);

  auto start_time = std::max(status.availability, toy->arrival_time);

  if (not Union::is_sanctioned_minute(start_time)
      or Union::is_last_sanctioned_minute(start_time)) {
    start_time = Union::next_sanctioned_minute(start_time);
  }

  this->add_job(elf_id, toy, start_time);
}

auto Schedule::add_job_avoid_unsanctioned(Elf::ID elf_id,
                                          std::shared_ptr<Toy> toy)
  -> void
{
  auto status = this->statuses.at(elf_id);

  auto start_time = std::max(status.availability, toy->arrival_time);
  auto eff = status.efficiency;
  auto processing_time = toy->processing_time / eff;

  auto offset = start_time % Union::DAY_DURATION;
  auto remaining = Union::END_OFFSET - offset;

  if (processing_time > Union::SANCTIONED_DURATION) {
    this->add_job(elf_id, toy, Union::next_sanctioned_start(start_time));
  } else if (Union::START_OFFSET <= offset and processing_time <= remaining) {
    this->add_job(elf_id, toy, start_time);
  } else {
    this->add_job(elf_id, toy, Union::next_sanctioned_start(start_time));
  }
}

auto Schedule::score() -> Score
{
  if (this->feasible) {
    long z = 0;
    for (auto elf_id : this->elves) {
      z = std::max(z, this->statuses.at(elf_id).stopped_at);
    }

    return Score(z * log(1 + this->elves.size()));
  } else {
    return Score(this->message);
  }
}

auto Schedule::status(Elf::ID elf_id) -> ElfStatus
{
  return this->statuses.at(elf_id);
}

auto Schedule::first_available() -> Elf::ID
{
  bool first = true;
  Elf::ID min_elf = 0;
  long min_avail = 0;
 
  for (auto &kv : this->statuses) {
    if (first or kv.second.availability < min_avail) {
      min_avail = kv.second.availability;
      min_elf = kv.first;
      first = false;
    }
  }

  return min_elf;
}

auto Schedule::get_toys(Elf::ID elf) -> Toys
{
  Toys toys;
  for (auto &instr : this->instructions.at(elf)) {
    toys.push_back(instr.toy);
  }

  return toys;
}

#include "instruction.hh"

Instruction::Instruction(long time, Elf::ID elf, std::shared_ptr<Toy> toy,
                         long duration)
  : time(time),
    elf(elf),
    toy(toy),
    duration(duration)
{
}

std::ostream& operator<< (std::ostream &out, Instruction &instr)
{
  auto days = boost::gregorian::date_duration(instr.time / Union::DAY_DURATION);
  auto date = time_util::epoch + days;

  auto hours = (instr.time % Union::DAY_DURATION) / Union::HOUR_DURATION;
  auto minutes = (instr.time % Union::DAY_DURATION) % Union::HOUR_DURATION;
      
  out << instr.toy->id
      << "," << instr.elf
      << "," << date.year()
      << " " << (int) date.month()
      << " " << date.day()
      << " " << hours
      << " " << minutes
      << "," << instr.duration;

  return out;
}

#ifndef SCHEDULE_HH
#define SCHEDULE_HH

#include <boost/date_time/gregorian/gregorian.hpp>

#include <vector>
#include <fstream>
#include <string>
#include <map>
#include <sstream>
#include <queue>

#include "instruction.hh"
#include "time_util.hh"
#include "score.hh"
#include "elf_status.hh"

/**
 * A Schedule is an ordered list of processing instructions for
 * creating toys.
 */
class Schedule
{
public:

  /**
   * Create an empty Schedule.
   */
  Schedule();

  /**
   * Create an empty Schedule with the given number of elves.
   */
  Schedule(long number_elves);
  
  /**
   * Read in a schedule from a file.
   */
  Schedule(std::ifstream &file, Toys &toys);

  /**
   * Create an empty Schedule with the given elves.
   */
  Schedule(std::vector<Elf::ID> elves);

  /**
   * Merge the instructions from the given schedule into this one.
   *
   * If an Elf has instructions in both the current Schedule and
   * Schedule a, it will receive only the instructions in Schedule a.
   */
  auto merge(Schedule const& a) -> void;

  /**
   * Schedule the Elf to make the given Toy at the next available
   * sanctioned time.
   */
  auto add_job_wait_for_sanctioned(Elf::ID elf_id, std::shared_ptr<Toy> toy) -> void;

  /**
   * Schedule the Elf to make the Toy at the next available sanctioned
   * time; if we cannot complete the toy without using unsanctioned
   * time then, delay until the next start of sanctioned time.
   */
  auto add_job_avoid_unsanctioned(Elf::ID elf_id,std::shared_ptr<Toy> toy) -> void;
  
  /**
   * Schedule the Elf to make the given Toy at the next available time.
   */
  auto add_job(Elf::ID elf_id, std::shared_ptr<Toy> toy) -> void;
  
  /**
   * Schedule the Elf to make the given Toy at the given time.
   */
  auto add_job(Elf::ID elf_id, std::shared_ptr<Toy> toy, long time) -> void;
  
  /**
   * Write the solution to the specified output stream.
   */
  auto write_solution(std::ostream &file) -> void;

  /**
   * Get the current status of an Elf.
   */
  auto status(Elf::ID elf_id) -> ElfStatus;

  /**
   * Get the first available Elf.
   */
  auto first_available() -> Elf::ID;

  /**
   * Score this schedule.
   */
  auto score() -> Score;

  /**
   * Get the list of all Toys processed by this Elf, in processing
   * order.
   *
   * This is a fairly expensive operation, as we need to generate this
   * list.
   */
  auto get_toys(Elf::ID) -> Toys;

  /**
   * The elves available.
   */
  std::vector<Elf::ID> elves;
private:

  std::map<Elf::ID, Toys> schedules;

  /// The instructions for each Elf.
  std::map<Elf::ID, std::vector<Instruction>> instructions;

  /// The current status of an Elf
  std::map<Elf::ID, ElfStatus> statuses;

  /// AN error message to report in the score, if any
  std::string message;

  /// Is this schedule feasible? We initially set this to true, and if
  /// an error is found while creating the schedule it is set to false.
  bool feasible;
};

#endif

#ifndef TOY_BIN_HH
#define TOY_BIN_HH

#include "toy.hh"

/**
 * A hash table mapping processing times to lists of toys with that
 * processing time.
 *
 * Whenever a Toy is requested and there is more than one toy
 * available, we return the one with the smallest release date.
 */
class ToyBin
{
public:
  /**
   * Construct a new ToyBin from the given toys.
   */
  ToyBin(const Toys &toys);

  /**
   * Construct an empty ToyBin.
   */
  ToyBin();

  /**
   * Return the number of toys with this size.
   */
  auto count(long p) const -> std::size_t;

  /**
   * Return and remove a toy with the given processing time.
   *
   * Throws a std::out_of_range exception if no such toy exists.
   */
  auto pop_toy(long p) -> std::shared_ptr<Toy>;

  /**
   * Return a list of all available processing times.
   */
  auto processing_times() const -> const std::vector<long>&;

  /**
   * Return and remove the Toy with the largest processing time among
   * toys with a processing time at most p.
   *
   * If no such toy exists, return the next smallest toy.
   *
   * Throws a std::out_of_range exception if the ToyBin is empty.
   */
  auto pop_largest_at_most(long p) -> std::shared_ptr<Toy>;
  
  /**
   * Return and remove the Toy with the largest processing time among
   * toys with a processing time at most p.
   *
   * If no such toy exists, return the next smallest toy.
   *
   * Throws a std::out_of_range exception if the ToyBin is empty.
   */
  auto pop_smallest_at_least(long p) -> std::shared_ptr<Toy>;

  /**
   * Return and remove the smallest available toy.
   */
  auto pop_smallest() -> std::shared_ptr<Toy>;
  
  /**
   * Return and remove the smallest available toy.
   */
  auto pop_largest() -> std::shared_ptr<Toy>;

  /**
   * Return the number of toys remaining in all bins.
   */
  auto size() const -> std::size_t;

  /**
   * Get all remaining toys, in no particular order.
   */
  auto toys() -> Toys;

private:
  /// The toy bins
  std::map<long, Toys> bins;

  /// The available toy sizes
  std::vector<long> sizes;
  std::vector<long> reverse_sizes;

  /// The number of toys available, of any size
  std::size_t _count;
};

#endif

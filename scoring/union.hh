#ifndef UNION_HH
#define UNION_HH

#include <algorithm>

/**
 * Functions used by the Union to compute working hours
 */
namespace Union {
  /**
   * Returns the next minute that starts a sanctioned period at or
   * after minute t.
   */
  auto next_sanctioned_start(long t) -> long;

  /**
   * Returns the next minute that ends a sanctioned period at or after
   * minute t.
   */
  auto next_sanctioned_end(long t) -> long;

  /**
   * Returns the day number of minute t. Day zero begins at minute zero.
   */
  auto get_day(long t) -> long;

  /**
   * Returns the total sanctioned time during the interval [a, b).
   */
  auto sanctioned_time_during(long a, long b) -> long;

  /**
   * Returns the total unsanctioned time during the interval [a, b).
   */
  auto unsanctioned_time_during(long a, long b) -> long;

  /**
   * Returns true if the given minute is sanctioned.
   */
  auto is_sanctioned_minute(long t) -> bool;

  /**
   * Returns the first sanctioned minute after time t.
   */
  auto next_sanctioned_minute(long t) -> long;

  /**
   * Finds the first minute an Elf can work after finishing a job at
   * time t with total rest requirement r.
   */
  auto start_work_period(long t, long r) -> long;

  /**
   * If t is 19:00 on a given day, return true.
   */
  auto is_last_sanctioned_minute(long t) -> bool;
  
  /// The duration of a sanctioned work day
  constexpr long SANCTIONED_DURATION = 10 * 60;

  /// The duration of a day
  constexpr long DAY_DURATION = 24 * 60;

  /// The starting time in minutes of a sanctioned period after the
  /// start of a day
  constexpr long START_OFFSET = 9 * 60;

  /// The ending time in minutes of a sanctioned period after the
  /// start of a day
  constexpr long END_OFFSET = START_OFFSET + SANCTIONED_DURATION;

  /// The duration of an hour, in minutes
  constexpr long HOUR_DURATION = 60;

}

#endif

#include "toy_bin.hh"

ToyBin::ToyBin()
  : _count(0)
{
}

ToyBin::ToyBin(Toys const& toys)
  : _count(0)
{
  for (auto &toy : toys) {
    auto p = toy->processing_time;

    if (this->bins.count(p) == 0) {
      Toys vec;
      this->bins.emplace(p, vec);
      this->sizes.push_back(p);
      this->reverse_sizes.push_back(p);
    }

    this->bins.at(p).push_back(toy);
    this->_count++;
  }
  
  // Sort each bin by release date
  for (auto &kv : this->bins) {
    std::sort(kv.second.begin(), kv.second.end(), Toy::release_date_gt);
  }

  // Sort the sizes, and create the reversed size list
  std::sort(this->sizes.begin(), this->sizes.end());
  std::sort(this->reverse_sizes.begin(), this->reverse_sizes.end(),
            std::greater<long>());
}

auto ToyBin::count(long p) const -> std::size_t
{
  if (this->bins.count(p)) {
    return this->bins.at(p).size();
  }

  return 0;
}

auto ToyBin::pop_toy(long p) -> std::shared_ptr<Toy>
{
  // This might throw
  auto &toys = this->bins.at(p);

  // Get the next toy in the list. We remove from the back both
  // because it is more efficient than removing from the front, and
  // because we've sorted the toys so that the last toy has the
  // earliest release date.
  auto toy = toys.back();
  toys.pop_back();
  this->_count--;

  // If we've emptied a bin, remove it
  if (toys.size() == 0) {
    this->bins.erase(p);
    auto pair = std::equal_range(this->sizes.begin(), this->sizes.end(), p);
    this->sizes.erase(pair.first);

    auto rpair = std::equal_range(this->reverse_sizes.begin(), this->reverse_sizes.end(), p, std::greater<long>());
    this->reverse_sizes.erase(rpair.first);
  }

  return toy;
}

auto ToyBin::processing_times() const -> const std::vector<long>&
{
  return this->sizes;
}

auto ToyBin::pop_largest_at_most(long p) -> std::shared_ptr<Toy>
{
  long ndx = 0;
  long size = this->sizes.size();
  for (; ndx + 1 < size and this->sizes.at(ndx + 1) <= p; ndx++) {}

  return this->pop_toy(this->sizes.at(ndx));
}

auto ToyBin::pop_smallest_at_least(long p) -> std::shared_ptr<Toy>
{
  long ndx = this->sizes.size() - 1;
  for (; ndx - 1 >= 0 and this->sizes.at(ndx - 1) >= p; ndx--) {}

  return this->pop_toy(this->sizes.at(ndx));
}

auto ToyBin::size() const -> std::size_t
{
  return this->_count;
}

auto ToyBin::toys() -> Toys
{
  Toys remaining;

  for (auto &kv : this->bins) {
    for (auto &toy : kv.second) {
      remaining.push_back(toy);
    }
  }
  
  return remaining;
}

auto ToyBin::pop_smallest() -> std::shared_ptr<Toy>
{
  return this->pop_toy(this->sizes.front());
}

auto ToyBin::pop_largest() -> std::shared_ptr<Toy>
{
  return this->pop_toy(this->sizes.back());
}

#ifndef ELF_HH
#define ELF_HH

#include <algorithm>
#include <math.h>
#include <memory>

#include "toy.hh"

/**
 * Functions related to Elf workers.
 */
namespace Elf
{
  typedef long ID;
  
  constexpr double MIN_EFFICIENCY = 0.25;
  constexpr double MAX_EFFICIENCY = 4.0;
  constexpr double SANCTIONED_RATE = 1.02;
  constexpr double UNSANCTIONED_RATE = 0.9;
};

#endif

#ifndef UTIL_HH
#define UTIL_HH

#include <boost/date_time/gregorian/gregorian.hpp>

#include "union.hh"

namespace time_util
{
  /**
   * Parse a string of the form YYYY MM DD HH mm into minutes since
   * the epoch.
   */
   auto get_minutes_from_date(std::string date_string) -> long;

  const boost::gregorian::date epoch (2014, 1, 1);
}

#endif

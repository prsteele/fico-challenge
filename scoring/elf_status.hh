#ifndef ELF_STATUS_HH
#define ELF_STATUS_HH

#include "elf.hh"
#include "union.hh"

class ElfStatus
{
public:
  /**
   * Construct a new ElfStatus for an Elf that has performed no work.
   */
  ElfStatus(Elf::ID id);

  /**
   * Construct a new ElfStatus with the given parameters.
   */
  ElfStatus(Elf::ID id, long availability, double efficiency, long started_at, long stopped_at);

  /// The ID of the Elf.
  Elf::ID id;
  
  /// The next time the Elf is available for work
  long availability;

  /// The current efficiency of this Elf
  double efficiency;

  /// The last time the Elf started working
  long started_at;

  /// The last time the Elf stopped working
  long stopped_at;

  auto static efficiency_after(double eff, double sanc, double unsanc) -> double;

  /**
   * Return the status of an Elf after a job.
   *
   * @param status The status of the Elf before the job.
   * @param start The start time of the job
   * @param toy The Toy to process.
   *
   * @return ElfStatus The new status of the Elf.
   */
  auto status_after(long start, std::shared_ptr<Toy> toy)
    -> ElfStatus;

  /**
   * Return the status of an Elf after completing a job at the next
   * available time.
   *
   * @param status The status of the Elf before the job.
   * @param start The start time of the job
   * @param toy The Toy to process.
   *
   * @return ElfStatus The new status of the Elf.
   */
  auto status_after(std::shared_ptr<Toy> toy)
    -> ElfStatus;

  
  static auto availability_lt(ElfStatus a, ElfStatus b) -> bool;
  static auto availability_gt(ElfStatus a, ElfStatus b) -> bool;
};

#endif

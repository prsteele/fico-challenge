#include "toy.hh"

Toy::Toy(long id, long arrival_time, long processing_time)
  : id(id),
    arrival_time(arrival_time),
    processing_time(processing_time)
{
}

auto Toy::load_toys(std::ifstream &file) -> Toys
{
  Toys toys;

  boost::gregorian::date epoch (2014, 1, 1);

  // Skip the header line
  std::string line;
  std::getline(file, line);

  // Read in all remaining lines
  while (std::getline(file, line)) {
    int end_id = line.find_first_of(',');
    int end_date = line.find_first_of(',', end_id + 1);

    // Process the ID
    long id = std::stol(line.substr(0, end_id));

    auto date_string = line.substr(end_id + 1, end_date - end_id);
    long release_time = time_util::get_minutes_from_date(date_string);

    long processing_time
      = std::stol(line.substr(end_date + 1, std::string::npos));

    toys.emplace_back(new Toy(id, release_time, processing_time));
  }

  return toys;
}

auto Toy::time_at(double efficiency) -> long
{
  return (long) ceil(this->processing_time / efficiency);
}

  
auto Toy::release_date_gt(std::shared_ptr<Toy> a, std::shared_ptr<Toy> b)
  -> bool
{
  return a->arrival_time > b->arrival_time;
}

auto Toy::release_date_lt(std::shared_ptr<Toy> a, std::shared_ptr<Toy> b)
  -> bool
{
  return a->arrival_time < b->arrival_time;
}

auto Toy::processing_time_gt(std::shared_ptr<Toy> a, std::shared_ptr<Toy> b) -> bool
{
  return a->processing_time > b->processing_time;
}
  
auto Toy::processing_time_lt(std::shared_ptr<Toy> a, std::shared_ptr<Toy> b) -> bool
{
  return a->processing_time < b->processing_time;
}

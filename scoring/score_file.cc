#include "score_file.hh"

auto main(int argc, char *argv[]) -> int
{
  if (argc != 3) {
    std::cout << "usage: score_file toys_file solution_file" << std::endl;
    return 1;
  }

  std::ifstream toys_file (argv[1], std::ifstream::in);
  std::ifstream soln_file (argv[2], std::ifstream::in);

  std::cout << "Reading toys file... ";
  auto toys = Toy::load_toys(toys_file);
  std::cout << "done" << std::endl;

  std::cout << "Reading solution file... ";
  Schedule schedule (soln_file, toys);
  std::cout << "done" << std::endl;
  
  auto score = schedule.score();

  if (score.feasible) {
    std::cout.precision(std::numeric_limits<double>::digits10);
    std::cout << score.objective_value << std::endl;
  } else {
    std::cout << "Infeasible solution: " << score.message << std::endl;
  }
}

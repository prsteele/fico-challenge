#include "score.hh"

Score::Score(double objective_value)
  : feasible(true),
    objective_value(objective_value),
    message("Feasible")
{
}

Score::Score(std::string message)
  : feasible(false),
    objective_value(0),
    message(message)
{
}

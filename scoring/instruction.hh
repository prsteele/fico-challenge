#ifndef INSTRUCTION_HH
#define INSTRUCTION_HH

#include <memory>

#include "elf.hh"
#include "toy.hh"

/**
 * An Instruction is an order for an Elf to begin processing a Toy at
 * a given time.
 */
class Instruction
{
public:
  /**
   * Constructs a new Instruction telling the Elf to make the Toy at
   * the given time.
   */
  Instruction(long time, Elf::ID elf, std::shared_ptr<Toy> toy, long duration);

  /// The start time of the Instruction, in minutes since the epoch.
  long time;

  /// The Elf doing the processing
  Elf::ID elf;

  /// The Toy being processed,
  std::shared_ptr<Toy> toy;

  /// The duration of this instruction
  long duration;

  /**
   * Write out this instruction to the buffer.
   *
   * The Toy begins processing at `time` and is processed for `duration`.
   */
  auto write(std::ostream &file) -> void;

  friend std::ostream& operator<< (std::ostream &out, Instruction &instr);
};

#endif

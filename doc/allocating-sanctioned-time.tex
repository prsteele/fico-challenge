\documentclass{article}

\usepackage{fullpage}
\usepackage{amsmath}
\usepackage{amsthm}

\newcommand{\ceil}[1]{\left\lceil#1\right\rceil}
\newcommand{\floor}[1]{\left\lfloor#1\right\rfloor}
\newtheorem{prop}{Prop}

\title{Allocating sanctioned time}
\author{Patrick Steele}
\date{\today}

\begin{document}

\maketitle

\section{Introduction}

Many improvements we have made in the FICO challenge has been by
observing that it is useful to distinguish between ``long'' and
``short'' jobs where, approximately, short jobs can be completed in a
single working day. Of course the efficiency of the elf completing the
short job decides whether the short job will be completed in a single
day, and so we simply define short jobs as all jobs with base
processing time less than $L$, for some $L \in [150, 2400]$.

As motivation for this distinction, there are 7978532 jobs with length
less than 2400 minutes and 2021468 jobs with length at least 2400
minutes; however, these long jobs have a total base processing time
of 24943756496 minutes, while the short jobs have a total processing
time of only 1060194269 minutes. Thus approximately 20\% of jobs
account for over 96\% of all processing time. This suggests that these
short jobs be used for nothing more than building up the efficiency of
an elf before processing a long job.

\section{Allocating sanctioned time}

Let a length $L$ be given, and suppose that the base processing time
of all small jobs is $S$ (in hours). Our goal is to distribute these
$S$ hours among the remaining $n$ long jobs. Let $s_1, \ldots, s_n$ be
given where $s_i \ge 0$ for all $i = 1, \ldots, n$ and $s_1 + \ldots +
s_n = S$ (here we allow $s_i \in \mathcal{R}_+$, despite the actual problem
having a resolution of only minutes). Then the $i$th long job with
length $\ell_i$ will be processed at efficiency at least
\begin{equation*}
  e_i = 0.25 \cdot 1.02^{s_i}
\end{equation*}
by scheduling all $s_i$ hours of short jobs only in sanctioned time
before long job $i$ on an elf of minimum starting efficiency. In
reality the efficiency $e_i$ is an over-estimate of the true
efficiency we will achieve since scheduling two jobs of length $s_i /
2$ sequentially leads to a lower efficiency than scheduling a single
job of length $s_i$.

Our goal is to choose $s_1, \ldots, s_n$ which will allow us to
minimize the makespan of our schedule; however, a natural first step
is to find $s_1, \ldots, s_n$ which leads to the smallest total
processing time required for long jobs. With this in mind, the
following optimization problem is natural.
\begin{equation*}
  \begin{alignedat}{2}
    \min & \quad && \sum_{i = 1}^n \frac{\ell_i}{0.25 \cdot 1.02^{s_i}} \\
    \text{s.t.} &&&
    \begin{alignedat}[t]{2}
      \sum_{i = 1}^n s_i - S & = 0 \\
      -s_i & \le 0 & \quad \forall i & = 1, \ldots, n.
    \end{alignedat}
  \end{alignedat}
\end{equation*}
However, this solution allows the efficiency of a machine to exceed
4.0, which we can fix with another $n$ inequalities. This leads us to
the following nonlinear convex optimization problem
\begin{equation}
  \begin{alignedat}{2}
    \min & \quad && \sum_{i = 1}^n \ell_i \cdot 1.02^{-s_i} \\
    \text{s.t.} &&&
    \begin{alignedat}[t]{2}
      \sum_{i = 1}^n s_i - S & = 0 \\
      -s_i & \le 0 & \quad \forall i & = 1, \ldots, n \\
      s_i - \frac{\ln(16)}{\ln(1.02)} & \le 0 & \quad \forall i & = 1,
      \ldots, n,
    \end{alignedat}
  \end{alignedat}
  \label{eq:allocation}
\end{equation}
where in the objective we have discarded the unnecessary $1 / 0.25$
constants.

To solve this problem we apply the KKT conditions. In particular, let
$\mu_1, \ldots, \mu_n$ be the dual variables associated with the first
inequality constraints, $\nu_1, \ldots, \nu_n$ be the dual variables
associated with the second inequality constraints, and let $\lambda$
be the dual variable associated with the equality constraint. Then we
need
\begin{equation}
  \label{eq:KKT}
  \begin{alignedat}{2}
    -\ln(1.02) 1.02^{-s_i} \ell_i & = - \mu_i + \nu_i + \lambda &
    \quad \forall i & = 1, \ldots, n \\
    \mu_i s_i & = 0 & \forall i & = 1, \ldots, n \\
    \nu_i \left(s_i - \frac{\ln(16)}{\ln(1.02)}\right) & = 0 &
    \forall i & = 1, \ldots, n, \\
    \mu_i, \nu_i & \ge 0 & \forall i & = 1, \ldots, n.
  \end{alignedat}
\end{equation}
Since all constraints are linear, these are necessary and sufficient
conditions for optimality.

We now prove a number of structural results for the optimal solution
to~\eqref{eq:allocation}. From here on out, we assume that jobs are
ordered so that $\ell_1 \le \ldots \le \ell_n$.

\begin{prop}
  \label{prop:ordering}
  There exists an optimal solution to~\eqref{eq:allocation} $s_1,
  \ldots, s_n$ where $s_1 \le \ldots \le s_n$.
\end{prop}

\begin{proof}
  Let an optimal solution $s_1, \ldots, s_n$ be given. If $s_1 \le
  \ldots \le s_n$, we are done; otherwise, there exists an index $i$
  such that $s_i > s_{i + 1}$. Consider a new solution $\hat s_1,
  \ldots \hat s_n$ that is identical to the original solution except
  $\hat s_i = s_{i + 1}$ and $\hat s_{i + 1} = s_i$. We show that the
  new solution is at least as good as the previous solution. Directly,
  \begin{equation*}
    \begin{aligned}
      \sum_{i = 1}^n \ell_i \cdot 1.02^{-s_i} - \sum_{i = 1}^n \ell_i
      \cdot 1.02^{-\hat s_i} & = \ell_i \cdot 1.02^{-s_i} + \ell_{i + 1}
      \cdot 1.02^{-s_{i + 1}} - \ell_i \cdot 1.02^{-\hat s_i} - \ell_{i + 1}
      \cdot 1.02^{-\hat s_{i + 1}} \\
      & = \ell_i \cdot 1.02^{-s_i} + \ell_{i + 1}
      \cdot 1.02^{-s_{i + 1}} - \ell_i \cdot 1.02^{-s_{i + 1}} - \ell_{i + 1}
      \cdot 1.02^{-s_i} \\
      & = \left(1.02^{-s_i} - 1.02^{-s_{i + 1}}\right) \left(\ell_i
        - \ell_{i + 1}\right).
    \end{aligned}
  \end{equation*}
  Since $s_i > s_{i + 1}$, $1.02^{-s_i} - 1.02^{-s_{i + 1}} \le 0$,
  while $\ell_i \le \ell_{i + 1}$. Thus
  \begin{equation*}
    \begin{aligned}
      \sum_{i = 1}^n \ell_i \cdot 1.02^{-s_i} - \sum_{i = 1}^n \ell_i
      \cdot 1.02^{-\hat s_i} & \ge 0,
    \end{aligned}
  \end{equation*}
  as required.
\end{proof}

\begin{prop}
  \label{prop:equal-size}
  Let an optimal solution $s_1, \ldots, s_n$ to~\eqref{eq:allocation}
  be given, and suppose that $\ell_i = \ell_j$. Then $s_i = s_j$.
\end{prop}

\begin{proof}
  Let any solution $s_1, \ldots, s_n$ be given where $s_i \not= s_j$
  (recall that $\ell_i = \ell_j$). Let $\hat s_1, \ldots, \hat s_n$
  be identical to the original solution except
  $\hat s_i = \hat s_j = (s_i + s_j) / 2 = \bar s_{i j}$. We show that
  the new solution is at least as good as the previous
  solution. Directly,
  \begin{equation*}
    \begin{aligned}
      \sum_{i = 1}^n \ell_i 1.02^{-s_i} - \sum_{i = 1}^n \ell_i
      1.02^{-\hat s_i} & = \ell_i 1.02^{-s_i} + \ell_j 1.02^{-s_j} -
      \ell_i 1.02^{-\hat s_i} - \ell_j 1.02^{-\hat s_j} \\
      & = \ell_i \left(1.02^{-s_i} + 1.02^{-s_j}
        - 1.02^{-\bar s_{i j}} - 1.02^{-\bar s_{i j}}\right) \\
      & = \ell_i \left(1.02^{-s_i} + 1.02^{-s_j} - 2 \cdot 1.02^{-s_i /
          2} \cdot 1.02^{-s_j / 2} \right) \\
      & = \ell_i \left( 1.02^{-s_i / 2} - 1.02^{-s_j / 2} \right)^2 \\
      & \ge 0,
    \end{aligned}
  \end{equation*}
  as required.
\end{proof}

From propositions~\ref{prop:ordering} and~\ref{prop:equal-size}, we
know that an optimal solution will be ordered and that all jobs of a
given size will be allocated an equal amount of time. This motivates a
scheme for computing the optimal $s_1, \ldots, s_n$. Recall that we
have bounds on $s_i$ of $0$ and $\ln(16) / \ln(1.02)$. Choose indices
$a$ and $b$, $a \le b$, such that $\ell_{a - 1} < \ell_a$ and
$\ell_b < \ell_{b + 1}$, where for convenience we define
$\ell_0 = -\infty$ and $\ell_{n + 1} = \infty$. We create an
allocation as follows:
\begin{equation*}
  s_i = \begin{cases}
    0, & i < a \\
    \xi, & a \le i \le b, \\
    \frac{\ln(16)}{\ln(1.02)}, & b < i,
  \end{cases}
\end{equation*}
where $0 < \xi < \ln(16) / \ln(1.02)$. From the KKT
conditions~\eqref{eq:KKT}, we then have that $\mu_i = 0$ for all
$i \ge a$, and $\nu_i = 0$ for all $i \le b$. Then $\mu_i = \nu_i$ for
$a \le i \le b$, and so from the first family of constraints
in~\eqref{eq:KKT} we have that
\begin{equation*}
  -\ln(1.02) 1.02^{-s_i} \ell_i = \lambda.
\end{equation*}
Thus for this solution to be feasible, it must be that $s_i = s_j$ for
all $a \le i < j \le b$; from~\eqref{eq:equal-size}, we know that this
requires that $\ell_a = \ell_b$. Thus there will at most one size
job that has more than zero but less than $\ln(16) / \ln(1.02)$
sanctioned time assigned to it; all smaller jobs will receive zero
sanctioned time, and all larger jobs will receive the maximum
sanctioned time.

Already this provides us with an efficient method for computing an
optimal solution. Group the jobs into distinct sizes
$\hat \ell_1 \le \ldots \hat \ell_{n'}$. For $i = 1, \ldots, n'$,
compute the objective value of the solution where all jobs of size
greater than $\hat \ell_i$ receive the maximum sanctioned time, all
jobs of shorter length receive no sanctioned time, and all jobs of
length $\hat \ell_i$ receive an equal portion of whatever sanctioned
time is left over. Finally, we choose the minimizing index $i$ such
that the resulting schedule uses exactly $S$ sanctioned time and use
that solution. This procedure takes polynomial time in the number of
distinct input lengths; however, we can improve on this
greatly. Specifically, let
$\beta = \floor{S / \left(\ln(16) / \ln(1.02)\right)}$. Then no more
than $\beta$ jobs can receive the maximum amount of sanctioned
time. If $\ell_{\beta - 1} < \ell_\beta$, then use $\beta$ as the
index $b$. Otherwise, choose $b$ as the smallest index greater than
$\beta$ such that $\ell_\beta < \ell_b$. Choose $a$ to be the smallest
index such that $\ell_a = \ell_\beta$. This gives us a feasible
solution that satisfies the KKT conditions, and so is optimal.
\end{document}
#include "greedy_factory.hh"

namespace po = boost::program_options;

auto GreedyFactory::generate(std::vector<std::string> const& heuristic_args)
  -> std::vector<std::unique_ptr<Heuristic>>
{
  po::options_description heuristic_options ("greedy options");

  heuristic_options.add_options()
    ("length", po::value<long>(), "the minimum length of long jobs")
    ("min-length", po::value<long>(), "the minimum value of the length value")
    ("max-length", po::value<long>(), "the maximum value of the length value")
    ("length-step", po::value<long>(), "the step size of length values")
    ("help", "print this message and exit");

  po::parsed_options parsed = po::command_line_parser(heuristic_args)
    .options(heuristic_options).run();

  po::variables_map vm;
  po::store(parsed, vm);

  std::vector<std::unique_ptr<Heuristic>> heuristics;

  if (vm.count("help")) {
    std::cout << Greedy::static_description() << std::endl << std::endl;
    std::cout << heuristic_options << std::endl;
    HeuristicFactory::HelpException ex ("Help printed");
    throw ex;
  }
    
  po::notify(vm);

  // The sets {--length} and {--min-length, --max-length, --length-step}
  // are mutually exclusive
  std::ostringstream length_msg_stream;
  length_msg_stream << "Must specificy either --length, xor all of --min-length, "
      << "--max-length, and --length-step";
  std::string length_message = length_msg_stream.str();

  if (vm.count("length") and (vm.count("min-length")
                              or vm.count("max-length")
                              or vm.count("length-step"))) {
  
    HeuristicFactory::OptionException ex (length_message);
    throw ex;
  }

  std::vector<long> lengths;
  if (vm.count("length")) {
    lengths.push_back(vm["length"].as<long>());
  } else if (vm.count("min-length")
             and vm.count("max-length")
             and vm.count("length-step")) {
    long min_length = vm["min-length"].as<long>();
    long max_length = vm["max-length"].as<long>();
    long length_step = vm["length-step"].as<long>();
    for (auto length = min_length; length <= max_length; length += length_step) {
      lengths.push_back(length);
    }
  } else {
    HeuristicFactory::OptionException ex (length_message);
    throw ex;
  }

  for (auto &length : lengths) {
    std::unique_ptr<Heuristic> p (new Greedy(length));
    heuristics.push_back(std::move(p));
  }
  
  return heuristics;
}

auto GreedyFactory::description() -> std::string
{
  return Greedy::static_description();
}


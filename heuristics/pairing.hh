#ifndef PAIRING_HH
#define PAIRING_HH

#include <queue>

#include "heuristic.hh"

class Pairing: public Heuristic
{
public:

  Pairing(long count);

  auto solve(Toys const& toys, Schedule &schedule) -> void;

  auto description() -> std::string;
  auto static static_description() -> std::string;

private:
  long count;

  std::string _description;
};

#endif

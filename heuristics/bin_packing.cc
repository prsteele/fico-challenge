#include "bin_packing.hh"

BinPacking(bool ascending)
  : ascending(ascending)
{
}

auto BinPacking::solve(Toys const& toys, Schedule &schedule) -> void
{

}

auto BinPacking::sizes(Toys const& toys, long bin_size) -> std::map<long, Toys>
{
  std::map<long, Toys> size_map;

  for (auto &toy : toys) {
    auto p = toy->processing_time;
    if (size_map.count(p) == 0) {
      Toys vec;
      size_map.emplace(p, vec);
    }

    size_map.at(p).push_back(toy);
  }

  return size_map;
}

auto BinPacking::configurations(Toys const& toys, long bin_size)
  -> std::vector<std::vector<int>>
{
  auto size_map = sizes(toys);
  std::vector<long> sizes;
  for (auto &kv : size_map) {
    sizes.push_back(kv.first);
  }
  std::sort(sizes.begin(), sizes.end());

  std::vector<long> configuration;
  std::vector<std::vector<long>> configurations;

  long i = 0;
  long total_size = 0;
  long max_total_size = 150;
  long size = sizes.at(i);
  while (i < sizes.size()) {
    if (total_size + size <= max_total_size) {
      configuration.at(i)++;
      total_size += size;

      configurations.push_back(configuration);
    } else {
      for (long j = i; j >= 0; j--) {
        auto j_size = sizes.at(j);
        total_size -= j_size * configuration.at(j);
        configuration.at(j) = 0;
      }
      i++;
    }
  }
}

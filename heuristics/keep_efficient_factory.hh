#ifndef KEEP_EFFICIENT_FACTORY_HH
#define KEEP_EFFICIENT_FACTORY_HH

#include <boost/program_options.hpp>

#include "heuristic_factory.hh"
#include "keep_efficient.hh"

class KeepEfficientFactory: public HeuristicFactory
{
  auto generate(std::vector<std::string> const& heuristic_args)
    -> std::vector<std::unique_ptr<Heuristic>>;

  auto description() -> std::string;
};

#endif

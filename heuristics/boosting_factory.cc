#include "boosting_factory.hh"

namespace po = boost::program_options;

auto BoostingFactory::generate(std::vector<std::string> const& heuristic_args)
  -> std::vector<std::unique_ptr<Heuristic>>
{
  po::options_description heuristic_options ("boosting options");

  heuristic_options.add_options()
    ("help", "print this message and exit");

  po::parsed_options parsed = po::command_line_parser(heuristic_args)
    .options(heuristic_options).run();

  po::variables_map vm;
  po::store(parsed, vm);

  std::vector<std::unique_ptr<Heuristic>> heuristics;

  if (vm.count("help")) {
    std::cout << Boosting::static_description() << std::endl << std::endl;
    std::cout << heuristic_options << std::endl;
    HeuristicFactory::HelpException ex ("Help printed");
    throw ex;
  }
    
  po::notify(vm);

  std::unique_ptr<Heuristic> p (new Boosting());
  heuristics.push_back(std::move(p));

  return heuristics;
}

auto BoostingFactory::description() -> std::string
{
  return Boosting::static_description();
}

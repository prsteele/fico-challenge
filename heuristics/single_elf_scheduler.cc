#include "single_elf_scheduler.hh"

SingleElfScheduler::SingleElfScheduler(long alpha, long beta, double keep_efficient_threshold)
  : heuristic(new ProcessMedium(alpha, beta, keep_efficient_threshold))
{
}

auto SingleElfScheduler::solve(Toys const& toys, Schedule &schedule) -> void
{
  Schedule _schedule (schedule.elves);
  
  this->heuristic->solve(toys, _schedule);

  std::cerr << "objective: " << _schedule.score().objective_value << std::endl;

  for (auto &elf : _schedule.elves) {
    auto elf_toys = _schedule.get_toys(elf);
    
    // Separate toys into bins
    ToyBin bin (elf_toys);

    while (bin.size() > 0) {
      auto status = schedule.status(elf);
      auto &sizes = bin.processing_times();

      std::shared_ptr<Toy> toy;
    
      if (status.efficiency < 0.34) {
        // Schedule a short job to boost efficiency
        toy = bin.pop_largest_at_most((long) floor(Union::DAY_DURATION * status.efficiency));
      } else {
        toy = bin.pop_toy(sizes.back());
      }

      schedule.add_job_avoid_unsanctioned(elf, toy);
    }
  }
}

auto SingleElfScheduler::description() -> std::string
{
  return "SingleElfScheduler";
}

auto SingleElfScheduler::static_description() -> std::string
{
  return "Finds a good schedule for the available jobs using a single Elf.";
}

#ifndef BIN_PACKING_HH
#define BIN_PACKING_HH

/**
 * Combine small jobs into larger chunks 
 */
class BinPacking: public Heuristic
{
public:
  /**
   * Construct a new bin packing instance where bins will be packed in
   * ascending order (if true) and descending order (if false).
   */
  BinPacking(bool ascending);
  
  auto virtual solve(Toys const& toys, Schedule &schedule) -> void;

private:
  /**
   * Return a vector of all configurations for the given set of toys.
   *
   * A configuration is a vector of counts of toys of each given size.
   */
  auto configurations(Toys const& toys, long bin_size) -> std::vector<std::vector<int>>;

  /**
   * Return a map between processing times and all toys with that
   * processing time.
   *
   */
  auto sizes(Toys const& toys, long bin_size) -> std::map<long, Toys>;

  /// Are the bins packed in ascending order?
  bool ascending;

};
  
#endif

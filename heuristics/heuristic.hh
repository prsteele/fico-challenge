#ifndef HEURISTIC_HH
#define HEURISTIC_HH

#include <memory>

#include "toy.hh"
#include "schedule.hh"

class Heuristic
{
public:
  /**
   * Compute a Schedule for the list of toys.
   */
  auto virtual solve(Toys const& toys) -> Schedule;

  /**
   * Compute a Schedule for the list of toys by augmenting the existing schedule.
   */
  auto virtual solve(Toys const& toys, Schedule &schedule) -> void = 0;

  /**
   * Return a succinct description of this particular heuristic,
   * including any relevant parameters.
   */
  auto virtual description() -> std::string =0;
};

#endif

#ifndef BOOSTING_FACTORY_HH
#define BOOSTING_FACTORY_HH

#include <boost/program_options.hpp>

#include "heuristic_factory.hh"
#include "boosting.hh"

class BoostingFactory: public HeuristicFactory
{
  auto generate(std::vector<std::string> const& heuristic_args)
    -> std::vector<std::unique_ptr<Heuristic>>;

  auto description() -> std::string;
};

#endif

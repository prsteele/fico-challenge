#include "list_processing_factory.hh"

namespace po = boost::program_options;

auto ListProcessingFactory::generate(std::vector<std::string> const& heuristic_args)
  -> std::vector<std::unique_ptr<Heuristic>>
{
  po::options_description heuristic_options ("list_processing options");

  heuristic_options.add_options()
    ("spt", "use the shortest processing time rule")
    ("lpt", "use the longest processing time rule")
    ("help", "print this message and exit");

  po::parsed_options parsed = po::command_line_parser(heuristic_args)
    .options(heuristic_options).run();

  po::variables_map vm;
  po::store(parsed, vm);

  std::vector<std::unique_ptr<Heuristic>> heuristics;

  if (vm.count("help")) {
    std::cout << ListProcessing::static_description() << std::endl << std::endl;
    std::cout << heuristic_options << std::endl;
    HeuristicFactory::HelpException ex ("Help printed");
    throw ex;
  }
    
  po::notify(vm);

  if (vm.count("spt") and vm.count("lpt")) {
    HeuristicFactory::OptionException ex ("Cannot specify both --spt and --lpt");
    throw ex;
  }

  if (vm.count("spt")) {
    std::unique_ptr<Heuristic> p (new ListProcessing(true));
    heuristics.push_back(std::move(p));
  } else if (vm.count("lpt")) {
    std::unique_ptr<Heuristic> p (new ListProcessing(false));
    heuristics.push_back(std::move(p));
  } else {
    std::unique_ptr<Heuristic> p (new ListProcessing());
    heuristics.push_back(std::move(p));
  }
  
  return heuristics;
}

auto ListProcessingFactory::description() -> std::string
{
  return ListProcessing::static_description();
}

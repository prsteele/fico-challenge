#include "keep_efficient_factory.hh"

namespace po = boost::program_options;

auto KeepEfficientFactory::generate(std::vector<std::string> const& heuristic_args)
  -> std::vector<std::unique_ptr<Heuristic>>
{
  po::options_description heuristic_options ("list_processing options");

  heuristic_options.add_options()
    ("length", po::value<long>(), "the short job threshold")
    ("min-length", po::value<long>(), "the minimum length of the short job threshold")
    ("max-length", po::value<long>(), "the maximum length of the short job threshold")
    ("length-step", po::value<long>(), "the step size of the short job threshold")
    ("alpha", po::value<double>(), "the efficiency threshold")
    ("min-alpha", po::value<double>(), "the minimum alpha of the short job threshold")
    ("max-alpha", po::value<double>(), "the maximum alpha of the short job threshold")
    ("alpha-step", po::value<double>(), "the step size of the short job threshold")
    ("help", "print this message and exit");

  po::parsed_options parsed = po::command_line_parser(heuristic_args)
    .options(heuristic_options).run();

  po::variables_map vm;
  po::store(parsed, vm);

  std::vector<std::unique_ptr<Heuristic>> heuristics;

  if (vm.count("help")) {
    std::cout << KeepEfficient::static_description() << std::endl << std::endl;
    std::cout << heuristic_options << std::endl;
    HeuristicFactory::HelpException ex ("Help printed");
    throw ex;
  }
    
  po::notify(vm);

  // The sets {--alpha} and {--min-alpha, --max-alpha, --alpha-step}
  // are mutually exclusive
  std::ostringstream alpha_msg_stream;
  alpha_msg_stream << "Must specificy either --alpha, xor all of --min-alpha, "
      << "--max-alpha, and --alpha-step";
  std::string alpha_message = alpha_msg_stream.str();

  std::vector<double> alphas;
  if (vm.count("alpha") and (vm.count("min-alpha")
                             or vm.count("max-alpha")
                             or vm.count("alpha-step"))) {
  
    HeuristicFactory::OptionException ex (alpha_message);
    throw ex;
  }

  if (vm.count("alpha")) {
    alphas.push_back(vm["alpha"].as<double>());
  } else if (vm.count("min-alpha")
             and vm.count("max-alpha")
             and vm.count("alpha-step")) {
    double min_alpha = vm["min-alpha"].as<double>();
    double max_alpha = vm["max-alpha"].as<double>();
    double alpha_step = vm["alpha-step"].as<double>();
    double epsilon = 1e-8;
    for (auto alpha = min_alpha; alpha <= max_alpha + epsilon;
         alpha += alpha_step) {
      alphas.push_back(alpha);
    }
  } else {
    HeuristicFactory::OptionException ex (alpha_message);
    throw ex;
  }

  // The sets {--length} and {--min-length, --max-length, --length-step}
  // are mutually exclusive
  std::ostringstream length_msg_stream;
  length_msg_stream << "Must specificy either --length, xor all of --min-length, "
      << "--max-length, and --length-step";
  std::string length_message = length_msg_stream.str();

  std::vector<long> lengths;
  if (vm.count("length") and (vm.count("min-length")
                             or vm.count("max-length")
                             or vm.count("length-step"))) {
  
    HeuristicFactory::OptionException ex (length_message);
    throw ex;
  }

  if (vm.count("length")) {
    lengths.push_back(vm["length"].as<long>());
  } else if (vm.count("min-length")
             and vm.count("max-length")
             and vm.count("length-step")) {
    long min_length = vm["min-length"].as<long>();
    long max_length = vm["max-length"].as<long>();
    long length_step = vm["length-step"].as<long>();
    for (auto length = min_length; length <= max_length;
         length += length_step) {
      lengths.push_back(length);
    }
  } else {
    HeuristicFactory::OptionException ex (length_message);
    throw ex;
  }

  for (auto &alpha : alphas) {
    for (auto &length : lengths) {
      std::unique_ptr<Heuristic> p (new KeepEfficient(length, alpha));
      heuristics.push_back(std::move(p));      
    }
  }

  return heuristics;
}

auto KeepEfficientFactory::description() -> std::string
{
  return KeepEfficient::static_description();
}

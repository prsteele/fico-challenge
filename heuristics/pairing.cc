#include "pairing.hh"

Pairing::Pairing(long count)
  : count(count)
{
  std::ostringstream desc;
  desc << "Pairing (" << count << ")";

  this->_description = desc.str();
}

auto Pairing::solve(Toys const& toys, Schedule &schedule) -> void
{
  auto elf_comp = [](ElfStatus a, ElfStatus b) {
    if (a.availability == b.availability) {
      return a.id > b.id;
    }
    
    return a.availability > b.availability;
  };

  std::priority_queue<ElfStatus, std::vector<ElfStatus>, decltype(elf_comp)>
    elves (elf_comp);

  for (auto elf : schedule.elves) {
    elves.push(schedule.status(elf));
  }
  
  auto toy_comp = [] (std::shared_ptr<Toy> a, std::shared_ptr<Toy> b) {
    return a->processing_time < b->processing_time;
  };
  
  Toys sorted_toys (toys.begin(), toys.end());
  std::sort(sorted_toys.begin(), sorted_toys.end(), toy_comp);

  auto short_toy = sorted_toys.begin();
  auto long_toy = sorted_toys.end();
  long_toy--;

  auto next_elf = elves.top();
  elves.pop();
  for (auto count = 0; short_toy < long_toy; count++) {
    if (count < this->count) {
      schedule.add_job_wait_for_sanctioned(next_elf.id, *short_toy);
      short_toy++;
    } else if (count == this->count) {
      schedule.add_job_wait_for_sanctioned(next_elf.id, *long_toy);
      long_toy--;
    } else {
      count = 0;
      elves.push(schedule.status(next_elf.id));
      next_elf = elves.top();
      elves.pop();
    }
  }
}

auto Pairing::static_description() -> std::string
{
  std::ostringstream desc;
  desc << "Sort all jobs by processing time, then pair (some number of) "
       << "short jobs with each long job and complete them sequentially.";

  return desc.str();
}

auto Pairing::description() -> std::string
{
  return this->_description;
}

#include "heuristic_util.hh"

auto heuristic_util::max_efficiency_for_length(double efficiency, long length)
  -> long
{
  auto a = log(Union::HOUR_DURATION * efficiency / length);
  auto b = -Union::HOUR_DURATION / log(Elf::SANCTIONED_RATE);
  return std::max(1L, (long) (a * b));
}

auto heuristic_util::sanctioned_time_for(double low_eff, double high_eff)
  -> long
{
  constexpr auto den = log(Elf::SANCTIONED_RATE);
  return Union::HOUR_DURATION * ceil(log(high_eff / low_eff) / den);
}

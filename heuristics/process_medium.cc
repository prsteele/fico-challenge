#include "process_medium.hh"

ProcessMedium::ProcessMedium(long alpha, long beta, double keep_efficient_threshold)
  : alpha(alpha),
    beta(beta),
    keep_efficient_threshold(keep_efficient_threshold)
{
  std::ostringstream desc;
  desc << "ProcessMedium (alpha=" << this->alpha
       << ", beta=" << this->beta
       << ", threshold=" << this->keep_efficient_threshold
       << ")";

  this->_description = desc.str();
}

auto ProcessMedium::solve(Toys const& toys,
                     Schedule &schedule)
  -> void
{

  Toys medium_toys_vec;
  Toys short_and_long_toys;

  // Make a ToyBin of medium jobs. Short jobs have processing times up
  // to min_proc_time, medium jobs have processing times above
  // min_proc_time and up to max_proc_time, and long jobs are all
  // remaining jobs.
  for (auto &toy : toys) {
    auto p = toy->processing_time;
    if (p <= this->alpha or this->beta < p) {
      short_and_long_toys.push_back(toy);
    } else {
      medium_toys_vec.push_back(toy);
    }
  }
  ToyBin medium_toys (medium_toys_vec);

  // Make a priority queue of elves, sorted by availability
  std::priority_queue<ElfStatus, std::vector<ElfStatus>,
                      decltype(&ElfStatus::availability_gt)>
    elves (&ElfStatus::availability_gt);

  for (auto elf : schedule.elves) {
    elves.push(schedule.status(elf));
  }

  while (medium_toys.size() > 0) {
    auto elf = elves.top();
    elves.pop();
    
    auto status = schedule.status(elf.id);
    auto offset = status.availability % Union::DAY_DURATION;
    auto remaining = (long) (std::max(0L, Union::END_OFFSET - offset) * status.efficiency);
    constexpr auto rat = -log(Elf::SANCTIONED_RATE) / log(Elf::UNSANCTIONED_RATE);
    auto max_unsanctioned = remaining * rat;
    auto breakeven = (long) (remaining + max_unsanctioned + 1);

    // Find the smallest toy at least as large as our breakeven value
    auto toy = medium_toys.pop_smallest_at_least(breakeven);
    schedule.add_job_avoid_unsanctioned(elf.id, toy);
    elves.push(schedule.status(elf.id));
  }

  double max_efficiency = 0;
  double min_efficiency = 4.1;
  while (elves.size() > 0) {
    auto eff = elves.top().efficiency;
    elves.pop();
    max_efficiency = std::max(max_efficiency, eff);
    min_efficiency = std::min(min_efficiency, eff);
  }
  std::cerr << "min efficiency: " << min_efficiency << std::endl;
  std::cerr << "max efficiency: " << max_efficiency << std::endl;

  KeepEfficient heuristic (this->alpha, this->keep_efficient_threshold);
  //Boosting heuristic (this->alpha);
  heuristic.solve(short_and_long_toys, schedule);
}

auto ProcessMedium::description() -> std::string
{
  return this->_description;
}

auto ProcessMedium::static_description() -> std::string
{
  std::ostringstream desc;
  desc << "Process all medium length jobs first. Medium length jobs cannot "
       << "be done in one sancioned day by elves with efficiency less than "
       << "some threshold alpha, and can be done by elves with efficiency"
       << "at most some threshold beta.";

  return desc.str();
}

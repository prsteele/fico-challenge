#ifndef HEURISTIC_UTIL_HH
#define HEURISTIC_UTIL_HH

#include <algorithm>
#include <math.h>

#include "union.hh"
#include "elf.hh"

namespace heuristic_util
{

  /**
   * Return the maximum amount of sanctioned time that should be used
   * on a job of a given length, given a current efficiency.
   *
   * If a toy has processing time L and an elf is currently at
   * efficiency f, then using A sanctioned time before processing the
   * toy results in a total of
   *
   *   A + sanctioned_rate^(-A / 60) * L / f
   *
   * minutes processing time. This function returns the value of A
   * that minimizes this function.
   *
   */
  auto max_efficiency_for_length(double efficiency, long length) -> long;

  /**
   * Return the amount of sanctioned time that must be worked in order
   * to transition from the low efficiency to the high efficiency.
   */
  auto sanctioned_time_for(double low_eff, double high_eff) -> long;

};

#endif

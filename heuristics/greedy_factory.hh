#ifndef GREEDY_FACTORY_HH
#define GREEDY_FACTORY_HH

#include <boost/program_options.hpp>

#include "heuristic_factory.hh"
#include "greedy.hh"

class GreedyFactory: public HeuristicFactory
{
  auto generate(std::vector<std::string> const& heuristic_args)
    -> std::vector<std::unique_ptr<Heuristic>>;

  auto description() -> std::string;
};

#endif

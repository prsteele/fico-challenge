#ifndef BOOSTING_HH
#define BOOSTING_HH

#include <numeric>
#include <queue>

#include "heuristic.hh"
#include "list_processing.hh"
#include "toy_bin.hh"

/**
 * This class assigns each long job some (possibly empty) set of short
 * jobs to be scheduled before it, then runs list processing.
 *
 * See doc/allocating-sanctioned-time.tex for details.
 *
 */
class Boosting: public Heuristic
{
public:
  auto solve(Toys const& toys, Schedule &schedule) -> void;

  auto static static_description() -> std::string;

  auto description() -> std::string;

private:
  auto compute_a_b_sizes(ToyBin const& bin, long total_sanctioned_time)
    -> std::pair<long, long>;
};

#endif

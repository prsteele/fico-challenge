#ifndef GREEDY_HH
#define GREEDY_HH

#include <queue>

#include "heuristic.hh"
#include "keep_efficient.hh"
#include "toy_bin.hh"

class Greedy: public Heuristic
{
public:
  Greedy(long min_length);
  
  auto virtual solve(Toys const& toys, Schedule &schedule) -> void;

  auto virtual description() -> std::string;

  auto static static_description() -> std::string;

private:
  long min_length;
};

#endif

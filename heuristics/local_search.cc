#include "local_search.hh"

auto LocalSearch::solve(Toys const& toys, Schedule &schedule) -> void
{
  // We can't actually generate a feasible schedule from scratch, so
  // rely on an inferior solver.
  ProcessMedium heuristic (0.34, 4.0, .34);
  if (toys.size() > 0) {
    heuristic.solve(toys, schedule);
  }

  // Find the Elf that stopped work latest
  auto comp = [](ElfStatus a, ElfStatus b) {
    return a.stopped_at < b.stopped_at;
  };

  std::vector<ElfStatus> elves;

  for (auto elf : schedule.elves) {
    elves.push_back(schedule.status(elf));
  }
  std::sort(elves.begin(), elves.end(), comp);

  std::cerr << schedule.score().objective_value << std::endl;
  for (auto &elf : schedule.elves) {
    // Find the earliest and latest elves
    auto elf_toys = schedule.get_toys(elf);

    std::vector<Elf::ID> sub_elves;
    sub_elves.push_back(elf);
    Schedule new_schedule (sub_elves);
    heuristic.solve(elf_toys, new_schedule);

    schedule.merge(new_schedule);
  }
}

auto LocalSearch::description() -> std::string
{
  return "LocalSearch";
}

auto LocalSearch::static_description() -> std::string
{
  return "A local search heuristic to improve existing solutions.";
}

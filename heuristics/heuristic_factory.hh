#ifndef HEURISTIC_FACTORY_HH
#define HEURISTIC_FACTORY_HH

#include <vector>
#include <memory>

#include "heuristic.hh"

/**
 * A HeuristicFactory is used to generate a vector of heuristics from
 * command-line arguments.
 */
class HeuristicFactory
{
public:

  /**
   * Generate all requested heuristics.
   */
  auto virtual generate(std::vector<std::string> const& heuristic_args)
    -> std::vector<std::unique_ptr<Heuristic>> = 0;

  /**
   * Return a human readable description of this heuristic.
   */
  auto virtual description() -> std::string = 0;

  /**
   * Thrown to indicate --help was passed
   */
  class HelpException: public std::runtime_error {
  public:
    HelpException(std::string const& what)
      : std::runtime_error(what)
    {
    }
  };


  /**
   * Thrown to indicate we should terminate from a bad option. The
   * error message is stored in the exception.
   */
  class OptionException: public std::runtime_error {
  public:
    OptionException(std::string const& what)
      : std::runtime_error(what)
    {
    }
  };

};

#endif

#ifndef LIST_PROCESSING_HH
#define LIST_PROCESSING_HH

#include <memory>
#include <vector>
#include <queue>

#include "heuristic.hh"
#include "toy.hh"

class ListProcessing: public Heuristic
{
public:
  /**
   * Process the list of toys in the order given.
   */
  ListProcessing();

  /**
   * Process the list of toys in ascending processing time.
   */
  ListProcessing(bool ascending);
  
  auto solve(Toys const& toys, Schedule &schedule) -> void;

  auto description() -> std::string;

  auto static static_description() -> std::string;

private:

  auto solve_no_sort(Toys const& toys, Schedule &schedule) -> void;
  
  bool ascending;
  bool descending;

  std::string _description;
};

#endif

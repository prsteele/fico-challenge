#ifndef LOCAL_SEARCH_HH
#define LOCAL_SEARCH_HH

#include "heuristic.hh"
#include "process_medium.hh"

/**
 * Perform local search heuristics on existing schedules.
 */
class LocalSearch: public Heuristic
{
public:
    auto solve(Toys const& toys, Schedule &schedule)-> void;

  auto description() -> std::string;

  auto static static_description() -> std::string;
};

#endif

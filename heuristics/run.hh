#ifndef RUN_HH
#define RUN_HH

#include <boost/program_options.hpp>

#include <iostream>
#include <memory>

#include "score.hh"
#include "heuristic.hh"
#include "heuristic_factory.hh"
#include "list_processing_factory.hh"
#include "keep_efficient_factory.hh"
#include "pairing_factory.hh"
#include "process_medium_factory.hh"
#include "local_search_factory.hh"
#include "boosting_factory.hh"
#include "greedy_factory.hh"
#include "single_elf_scheduler_factory.hh"

auto main(int argc, char *argv[]) -> int;

#endif

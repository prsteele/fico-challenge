#include "boosting.hh"

auto Boosting::solve(Toys const& toys, Schedule &schedule) -> void
{
  ToyBin bin (toys);

  // Make a priority queue of elves, sorted by availability
  std::priority_queue<ElfStatus, std::vector<ElfStatus>,
                      decltype(&ElfStatus::availability_gt)>
    elves (&ElfStatus::availability_gt);
  for (auto &elf : schedule.elves) {
    elves.push(schedule.status(elf));
  }
  
  //
  // Sort the Toys into long and short groups. The long jobs should be
  // sorted in increasing order (to match the documentation). Short
  // jobs are sorted according to length.
  //
  Toys long_toys;
  std::map<long, Toys> short_toys;

  // The maximum length a job can be before reducing efficiency
  auto rat = log(Elf::SANCTIONED_RATE) / log(Elf::UNSANCTIONED_RATE);
  auto max_unsanctioned = -Union::SANCTIONED_DURATION * rat;
  auto max_length = max_unsanctioned + Union::SANCTIONED_DURATION;
    
  auto sanctioned_time = 0L;
  for (auto &toy : toys) {
    if (toy->processing_time <= max_length) {
      sanctioned_time += toy->processing_time;
    }
  }

  while (bin.size() > 0) {
    auto elf = elves.top();
    elves.pop();

    if (elf.efficiency < 4.0) {
      auto offset = elf.availability % Union::DAY_DURATION;
      auto remaining = std::min(Union::SANCTIONED_DURATION,
                                std::max(0L, Union::END_OFFSET - offset));

      if (remaining == 0) {
        remaining = Union::SANCTIONED_DURATION * elf.efficiency;
      }

      auto required_hours =
        log(Elf::MAX_EFFICIENCY / elf.efficiency) / log(Elf::SANCTIONED_RATE);
      auto required = (long) required_hours * Union::HOUR_DURATION;

      auto target = std::min(remaining, required);

      auto toy = bin.pop_largest_at_most(target);

      schedule.add_job_avoid_unsanctioned(elf.id, toy);
    } else {
      auto toy = bin.pop_largest();
      schedule.add_job(elf.id, toy);
    }

    elves.push(schedule.status(elf.id));
  }
}

auto Boosting::compute_a_b_sizes(ToyBin const& bin, long total_sanctioned_time)
  -> std::pair<long, long>
{
  constexpr auto rat = Union::HOUR_DURATION * log(Elf::MAX_EFFICIENCY / Elf::MIN_EFFICIENCY) /
    log(Elf::SANCTIONED_RATE);

  long z = floor(((double) total_sanctioned_time) / rat);

  auto &sizes = bin.processing_times();
  long count = 0;
  auto b = sizes.back() + 1;
  for (auto it = sizes.rbegin(); it != sizes.rend(); it++) {
    auto p = *it;
    long pcount = bin.count(p);
    
    if (count + pcount < z) {
      count += pcount;
      b = p;
    }
  }

  auto a = b;
  if (count < z) {
    a = b - 1;
  }

  return std::make_pair(a, b);
}

auto Boosting::static_description() -> std::string
{
  std::ostringstream desc;
  desc << "We assign short jobs to boost the efficiency that long "
       << "jobs are processed at; see the documentation in "
       << "doc/allocating-sanctioned-time.tex";

  return desc.str();
}

auto Boosting::description() -> std::string
{
  return "Boosting";
}

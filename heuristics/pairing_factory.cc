#include "pairing_factory.hh"

namespace po = boost::program_options;

auto PairingFactory::generate(std::vector<std::string> const& heuristic_args)
    -> std::vector<std::unique_ptr<Heuristic>>
{
  po::options_description heuristic_options ("list_processing options");

  heuristic_options.add_options()
    ("count", po::value<long>(),
     "the number of short jobs to pair with each long job")
    ("min-count", po::value<long>(),
     "the minimum number of short jobs to pair with each long job")
    ("max-count", po::value<long>(),
     "the maximum number of short jobs to pair with each long job")
    ("count-step", po::value<long>(),
     "the step size of the number of short jobs to pair with each long job")
    ("help", "print this message and exit");

  po::parsed_options parsed = po::command_line_parser(heuristic_args)
    .options(heuristic_options).run();

  po::variables_map vm;
  po::store(parsed, vm);

  if (vm.count("help")) {
    std::cout << Pairing::static_description() << std::endl << std::endl;
    std::cout << heuristic_options << std::endl;
    HeuristicFactory::HelpException ex ("Help printed");
    throw ex;
  }
    
  po::notify(vm);

  std::ostringstream count_msg_stream;
  count_msg_stream << "Must specificy either --count, xor all of --min-count, "
      << "--max-count, and --count-step";
  std::string count_message = count_msg_stream.str();

  if (vm.count("count") and (vm.count("min-count")
                             or vm.count("max-count")
                             or vm.count("count-step"))) {
  
    HeuristicFactory::OptionException ex (count_message);
    throw ex;
  }

  std::vector<long> counts;
  if (vm.count("count")) {
    counts.push_back(vm["count"].as<long>());
  } else if (vm.count("min-count")
             and vm.count("max-count")
             and vm.count("count-step")) {
    long min_count = vm["min-count"].as<long>();
    long max_count = vm["max-count"].as<long>();
    long count_step = vm["count-step"].as<long>();
    for (auto count = min_count; count <= max_count;
         count += count_step) {
      counts.push_back(count);
    }
  } else {
    HeuristicFactory::OptionException ex (count_message);
    throw ex;
  }
  
  std::vector<std::unique_ptr<Heuristic>> heuristics;
  for (auto &count : counts) {
    std::unique_ptr<Heuristic> p (new Pairing(count));
    heuristics.push_back(std::move(p));
  }

  return heuristics;
}

auto PairingFactory::description() -> std::string
{
  return Pairing::static_description();
}

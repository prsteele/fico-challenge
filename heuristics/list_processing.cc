#include "list_processing.hh"

ListProcessing::ListProcessing()
  : ascending(false),
    descending(false)
{
  if (this->ascending) {
    this->_description = "ListProcessing (spt)";
  } else if (this->descending) {
    this->_description = "ListProcessing (lpt)";
  } else {
    this->_description = "ListProcessing";
  }
}

ListProcessing::ListProcessing(bool ascending)
  : ascending(ascending),
    descending(!ascending)
{
  if (this->ascending) {
    this->_description = "ListProcessing (spt)";
  } else if (this->descending) {
    this->_description = "ListProcessing (lpt)";
  } else {
    this->_description = "ListProcessing";
  }
}

auto ListProcessing::solve(Toys const& toys, Schedule &schedule) -> void
{
  if (this->ascending || this->descending) {
    Toys sorted_toys (toys.begin(), toys.end());

    auto toy_comp = [] (std::shared_ptr<Toy> a, std::shared_ptr<Toy> b) {
      return a->processing_time < b->processing_time;
    };
    
    std::sort(sorted_toys.begin(), sorted_toys.end(), toy_comp);

    if (descending) {
      std::reverse(sorted_toys.begin(), sorted_toys.end());
    }

    ListProcessing no_sort;

    this->solve_no_sort(sorted_toys, schedule);
  } else {
    this->solve_no_sort(toys, schedule);
  }
}

auto ListProcessing::solve_no_sort(Toys const& toys, Schedule &schedule) -> void
{
  auto comp = [](ElfStatus a, ElfStatus b) {
    if (a.availability == b.availability) {
      return a.id > b.id;
    }
    
    return a.availability > b.availability;
  };

  std::priority_queue<ElfStatus, std::vector<ElfStatus>, decltype(comp)>
    elves (comp);

  for (auto elf : schedule.elves) {
    elves.push(schedule.status(elf));
  }

  for (auto &toy : toys) {
    auto elf = elves.top();
    elves.pop();

    schedule.add_job_wait_for_sanctioned(elf.id, toy);

    elves.push(schedule.status(elf.id));
  }
}

auto ListProcessing::static_description() -> std::string
{
  std::ostringstream desc;
  desc << "All jobs are maintained in some order. Whenever a machine "
       << "becomes idle the next job is assigned to it.";

  return desc.str();
}

auto ListProcessing::description() -> std::string
{
  return this->_description;
}

#include "heuristic.hh"

auto Heuristic::solve(Toys const& toys) -> Schedule
{
  long number_elves = 900;
  Schedule schedule (number_elves);
  this->solve(toys, schedule);
  return schedule;
}

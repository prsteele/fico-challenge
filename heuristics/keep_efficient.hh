#ifndef KEEP_EFFICIENT_HH
#define KEEP_EFFICIENT_HH

#include <set>
#include <vector>
#include <deque>

#include "heuristic.hh"
#include "list_processing.hh"
#include "heuristic_util.hh"
#include "toy_bin.hh"

/**
 * This Heuristic splits jobs into long jobs and small jobs. Small
 * jobs are used to get elves up to alpha efficiency, after which they
 * process long jobs.
 */
class KeepEfficient: public Heuristic
{
public:
  /**
   * Create a new Heuristic where short jobs have length at most
   * `length', and we process short jobs as long as an Elf has
   * efficiency below alpha.
   */
  KeepEfficient(long length, double alpha);

  auto solve(Toys const& toys, Schedule &schedule) -> void;

  auto description() -> std::string;
  auto static static_description() -> std::string;

private:

  /// The maximum length of short jobs
  long length;

  /// The minimum efficiency with which we process short jobs
  double alpha;

  std::string _description;
};

#endif

#ifndef SINGLE_ELF_SCHEDULER_HH
#define SINGLE_ELF_SCHEDULER_HH

#include "heuristic.hh"
#include "process_medium.hh"

/**
 * Create a schedule using only a single Elf. With this in mind, this
 * heuristic should only be called with a subset of all available
 * jobs.
 */
class SingleElfScheduler: public Heuristic
{
public:
  SingleElfScheduler(long alpha, long beta, double keep_efficient_threshold);
  
  auto solve(Toys const& toys, Schedule &schedule) -> void;

  auto description() -> std::string;

  auto static static_description() -> std::string;

private:
  std::unique_ptr<Heuristic> heuristic;
};

#endif

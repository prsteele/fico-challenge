#ifndef PROCESS_MEDIUM_HH
#define PROCESS_MEDIUM_HH

#include <queue>

#include "heuristic.hh"
#include "keep_efficient.hh"
#include "boosting.hh"
#include "toy_bin.hh"

class ProcessMedium: public Heuristic
{
public:
  ProcessMedium(long alpha, long beta, double keep_efficient_threshold);
  
  auto solve(Toys const& toys, Schedule &schedule) -> void;

  auto description() -> std::string;
  auto static static_description() -> std::string;
  
private:
  /// Short jobs have processing time up alpha
  long alpha;

  /// Medium jobs have processing time up beta
  long beta;

  // The threshold for the KeepEfficient heuristic
  double keep_efficient_threshold;

  std::string _description;

  int count = 0;
};

#endif

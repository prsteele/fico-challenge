#ifndef LOCAL_SEARCH_FACTORY_HH
#define LOCAL_SEARCH_FACTORY_HH

#include <boost/program_options.hpp>

#include "heuristic_factory.hh"
#include "local_search.hh"

class LocalSearchFactory: public HeuristicFactory
{
public:
  auto generate(std::vector<std::string> const& heuristic_args)
    -> std::vector<std::unique_ptr<Heuristic>> ;

  auto description() -> std::string;
};

#endif

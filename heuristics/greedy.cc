#include "greedy.hh"

Greedy::Greedy(long min_length)
  : min_length(min_length)
{
}

auto Greedy::solve(Toys const& toys, Schedule &schedule) -> void
{
  ToyBin bins (toys);

  // Make a priority queue of elves, sorted by availability
  std::priority_queue<ElfStatus, std::vector<ElfStatus>,
                      decltype(&ElfStatus::availability_gt)>
    elves (&ElfStatus::availability_gt);

  for (auto elf : schedule.elves) {
    elves.push(schedule.status(elf));
  }

  // Schedule as many jobs as we can, always scheduling the longest
  // job that we can efficiently schedule. Halt when we only have very
  // long jobs and very short jobs.
  while (bins.size() > 0) {
    auto elf = elves.top();
    elves.pop();
    
    auto status = schedule.status(elf.id);
    auto offset = status.availability % Union::DAY_DURATION;
    auto remaining = (long) (std::max(0L, Union::END_OFFSET - offset) * status.efficiency);
    constexpr auto rat = -log(Elf::SANCTIONED_RATE) / log(Elf::UNSANCTIONED_RATE);
    auto max_unsanctioned = remaining * rat;
    auto breakeven = (long) (remaining + max_unsanctioned);

    // Find the smallest toy at least as large as our breakeven value
    auto toy = bins.pop_largest_at_most(breakeven);

    if (toy->processing_time < this->min_length and toy->processing_time < breakeven) {
      // Process all remaining jobs using another heuristic

      auto remaining = bins.toys();
      remaining.push_back(toy);

      std::cerr << "Bailing out with " << remaining.size() << " jobs remaining" << std::endl;
      
      KeepEfficient heuristic (this->min_length, 0.34);
      heuristic.solve(remaining, schedule);
      break;
    }
    
    schedule.add_job_avoid_unsanctioned(elf.id, toy);
    elves.push(schedule.status(elf.id));
  }
}

auto Greedy::description() -> std::string
{
  std::ostringstream msg;
  msg << "Greedy (" << this->min_length << ")";

  return msg.str();
}

auto Greedy::static_description() -> std::string
{
  return "Always processes the longest job that can be efficientl computed";
}

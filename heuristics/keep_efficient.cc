#include "keep_efficient.hh"

KeepEfficient::KeepEfficient(long length, double alpha)
  : length(length),
    alpha(alpha)
{
  std::ostringstream desc;
  desc << "KeepEfficient (alpha=" << this->alpha
       << ", length=" << this->length << ")";

  this->_description = desc.str();
}

auto KeepEfficient::solve(Toys const& toys, Schedule &schedule) -> void
{
  // Make a priority queue of elves, sorted by availability
  std::priority_queue<ElfStatus, std::vector<ElfStatus>,
                      decltype(&ElfStatus::availability_gt)>
    high_efficiency_elves (&ElfStatus::availability_gt);

  std::priority_queue<ElfStatus, std::vector<ElfStatus>,
                      decltype(&ElfStatus::availability_gt)>
    low_efficiency_elves (&ElfStatus::availability_gt);

  for (auto elf : schedule.elves) {
    if (schedule.status(elf).efficiency >= this->alpha) {
      high_efficiency_elves.push(schedule.status(elf));
    } else {
      low_efficiency_elves.push(schedule.status(elf));
    }
  }

  // Separate the toys into equally sized piles
  ToyBin bin (toys);

  // Run until we're empty
  while (bin.size() > 0) {
    auto &sizes = bin.processing_times();

    Elf::ID elf_id;
    
    if (high_efficiency_elves.size() > 0) {
      // If the largest job left is a small job, bail out
      if (sizes.back() <= this->length) {
        break;
      }
      
      // We have a high-efficiency elf, so do a long toy
      auto elf = high_efficiency_elves.top();
      high_efficiency_elves.pop();

      elf_id = elf.id;
      auto toy = bin.pop_largest();

      schedule.add_job(elf_id, toy);
    } else {
      // If the smallest job is a long job, bail out
      if (sizes.front() > this->length) {
        break;
      }
      
      // We have no high-efficiency elves, so do a short toy
      auto elf = low_efficiency_elves.top();
      low_efficiency_elves.pop();

      auto offset = elf.availability % Union::DAY_DURATION;
      auto remaining = std::max(0L, Union::END_OFFSET - offset);
      
      elf_id = elf.id;
      auto toy = bin.pop_largest_at_most(remaining * elf.efficiency);

      schedule.add_job_avoid_unsanctioned(elf_id, toy);
    }

    // Determine if the Elf is now high- or low-efficiency
    auto new_status = schedule.status(elf_id);
    if (new_status.efficiency >= this->alpha) {
      high_efficiency_elves.push(new_status);
    } else {
      low_efficiency_elves.push(new_status);
    }

  }

  // We might have terminated early, in which case we finish with list processing
  if (bin.size() > 0) {
    ListProcessing heuristic (false);

    heuristic.solve(bin.toys(), schedule);
  }
}

auto KeepEfficient::static_description() -> std::string
{
  std::ostringstream desc;
  desc << "Jobs are split into long and short jobs. Short jobs " 
       << "are used to make machines more efficient, while efficient "
       << "machines are used to process long jobs.";

  return desc.str();
}

auto KeepEfficient::description() -> std::string
{
  return this->_description;
}

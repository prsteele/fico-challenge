#include "process_medium_factory.hh"

namespace po = boost::program_options;

auto ProcessMediumFactory::generate(std::vector<std::string> const& heuristic_args)
  -> std::vector<std::unique_ptr<Heuristic>>
{
  po::options_description heuristic_options ("process_medium options");

  heuristic_options.add_options()
    ("alpha", po::value<long>(), "the maximum efficiency for short jobs")
    ("min-alpha", po::value<long>(), "the minimum value of the alpha value")
    ("max-alpha", po::value<long>(), "the maximum value of the alpha value")
    ("alpha-step", po::value<long>(), "the step size of alpha values")
    ("beta", po::value<long>(), "the maximum efficiency for medium jobs")
    ("min-beta", po::value<long>(), "the minimum value of the beta value")
    ("max-beta", po::value<long>(), "the maximum value of the beta value")
    ("beta-step", po::value<long>(), "the step size of beta values")
    ("threshold", po::value<double>(), "the maximum efficiency for medium jobs")
    ("min-threshold", po::value<double>(), "the minimum value of the threshold value")
    ("max-threshold", po::value<double>(), "the maximum value of the threshold value")
    ("threshold-step", po::value<double>(), "the step size of threshold values")
    ("help", "print this message and exit");

  po::parsed_options parsed = po::command_line_parser(heuristic_args)
    .options(heuristic_options).run();

  po::variables_map vm;
  po::store(parsed, vm);

  std::vector<std::unique_ptr<Heuristic>> heuristics;

  if (vm.count("help")) {
    std::cout << ProcessMedium::static_description() << std::endl << std::endl;
    std::cout << heuristic_options << std::endl;
    HeuristicFactory::HelpException ex ("Help printed");
    throw ex;
  }
    
  po::notify(vm);

  // The sets {--alpha} and {--min-alpha, --max-alpha, --alpha-step}
  // are mutually exclusive
  std::ostringstream alpha_msg_stream;
  alpha_msg_stream << "Must specificy either --alpha, xor all of --min-alpha, "
      << "--max-alpha, and --alpha-step";
  std::string alpha_message = alpha_msg_stream.str();

  if (vm.count("alpha") and (vm.count("min-alpha")
                             or vm.count("max-alpha")
                             or vm.count("alpha-step"))) {
  
    HeuristicFactory::OptionException ex (alpha_message);
    throw ex;
  }

  std::vector<long> alphas;
  if (vm.count("alpha")) {
    alphas.push_back(vm["alpha"].as<long>());
  } else if (vm.count("min-alpha")
             and vm.count("max-alpha")
             and vm.count("alpha-step")) {
    long min_alpha = vm["min-alpha"].as<long>();
    long max_alpha = vm["max-alpha"].as<long>();
    long alpha_step = vm["alpha-step"].as<long>();
    for (auto alpha = min_alpha; alpha <= max_alpha; alpha += alpha_step) {
      alphas.push_back(alpha);
    }
  } else {
    HeuristicFactory::OptionException ex (alpha_message);
    throw ex;
  }

  // The sets {--beta} and {--min-beta, --max-beta, --beta-step}
  // are mutually exclusive
  std::ostringstream beta_msg_stream;
  beta_msg_stream << "Must specificy either --beta, xor all of --min-beta, "
      << "--max-beta, and --beta-step";
  std::string beta_message = beta_msg_stream.str();

  if (vm.count("beta") and (vm.count("min-beta")
                             or vm.count("max-beta")
                             or vm.count("beta-step"))) {
  
    HeuristicFactory::OptionException ex (beta_message);
    throw ex;
  }

  std::vector<long> betas;
  if (vm.count("beta")) {
    betas.push_back(vm["beta"].as<long>());
  } else if (vm.count("min-beta")
             and vm.count("max-beta")
             and vm.count("beta-step")) {
    long min_beta = vm["min-beta"].as<long>();
    long max_beta = vm["max-beta"].as<long>();
    long beta_step = vm["beta-step"].as<long>();
    for (auto beta = min_beta; beta <= max_beta; beta += beta_step) {
      betas.push_back(beta);
    }
  } else {
    HeuristicFactory::OptionException ex (beta_message);
    throw ex;
  }

  // The sets {--threshold} and {--min-threshold, --max-threshold, --threshold-step}
  // are mutually exclusive
  std::ostringstream threshold_msg_stream;
  threshold_msg_stream << "Must specificy either --threshold, xor all of --min-threshold, "
      << "--max-threshold, and --threshold-step";
  std::string threshold_message = threshold_msg_stream.str();

  if (vm.count("threshold") and (vm.count("min-threshold")
                             or vm.count("max-threshold")
                             or vm.count("threshold-step"))) {
  
    HeuristicFactory::OptionException ex (threshold_message);
    throw ex;
  }

  std::vector<double> thresholds;
  if (vm.count("threshold")) {
    thresholds.push_back(vm["threshold"].as<double>());
  } else if (vm.count("min-threshold")
             and vm.count("max-threshold")
             and vm.count("threshold-step")) {
    double min_threshold = vm["min-threshold"].as<double>();
    double max_threshold = vm["max-threshold"].as<double>();
    double threshold_step = vm["threshold-step"].as<double>();
    for (auto threshold = min_threshold; threshold <= max_threshold; threshold += threshold_step) {
      thresholds.push_back(threshold);
    }
  } else {
    HeuristicFactory::OptionException ex (threshold_message);
    throw ex;
  }

  for (auto &alpha : alphas) {
    for (auto &beta : betas) {
      for (auto &threshold : thresholds) {
        std::unique_ptr<Heuristic> p (new ProcessMedium(alpha, beta, threshold));
        heuristics.push_back(std::move(p));
      }
    }
  }
  
  return heuristics;
}

auto ProcessMediumFactory::description() -> std::string
{
  return ProcessMedium::static_description();
}

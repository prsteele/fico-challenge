#include "run.hh"

namespace po = boost::program_options;

auto main(int argc, char *argv[]) -> int
{
  //
  // Process the command line arguments
  //

  std::ostringstream usage;
  usage << "usage: " << argv[0] << " TOYS HEURISTIC [HEURISTIC-OPTIONS]";

  po::options_description general_options ("General options");
  general_options.add_options()
    ("help", "print this message and quit")
    ("list", "list all available heuristics")
    // ("log-file", po::value<std::string>(), "specify the log file")
    // ("solution-file", po::value<std::string>(), "specify the solution file")
    ("toys", po::value<std::string>()->required(), "the toys file");

  po::options_description hidden_options ("");
  hidden_options.add_options()
    ("heuristic", po::value<std::string>()->required(), "the heuristic to run")
    ("heuristic_args", po::value<std::vector<std::string>>(), "the options to the heuristic");

  po::options_description full_options ("");
  full_options.add(general_options).add(hidden_options);

  po::positional_options_description pos;
  pos.add("heuristic", 1);
  pos.add("heuristic_args", -1);

  po::variables_map vm;

  po::parsed_options parsed = po::command_line_parser(argc, argv)
    .options(full_options)
    .positional(pos)
    .allow_unregistered()
    .run();
  
  po::store(parsed, vm);

  std::map<std::string, std::unique_ptr<HeuristicFactory>> heuristic_map;
  std::unique_ptr<HeuristicFactory> lp_uni (new ListProcessingFactory());
  std::unique_ptr<HeuristicFactory> ke_uni (new KeepEfficientFactory());
  std::unique_ptr<HeuristicFactory> pa_uni (new PairingFactory());
  std::unique_ptr<HeuristicFactory> pm_uni (new ProcessMediumFactory());
  std::unique_ptr<HeuristicFactory> ls_uni (new LocalSearchFactory());
  std::unique_ptr<HeuristicFactory> bo_uni (new BoostingFactory());
  std::unique_ptr<HeuristicFactory> gr_uni (new GreedyFactory());
  std::unique_ptr<HeuristicFactory> se_uni (new SingleElfSchedulerFactory());
  heuristic_map.insert(std::make_pair("list_processing", std::move(lp_uni)));
  heuristic_map.insert(std::make_pair("keep_efficient", std::move(ke_uni)));
  heuristic_map.insert(std::make_pair("pairing", std::move(pa_uni)));
  heuristic_map.insert(std::make_pair("process_medium", std::move(pm_uni)));
  heuristic_map.insert(std::make_pair("local_search", std::move(ls_uni)));
  heuristic_map.insert(std::make_pair("boosting", std::move(bo_uni)));
  heuristic_map.insert(std::make_pair("greedy", std::move(gr_uni)));
  heuristic_map.insert(std::make_pair("single_elf_scheduler", std::move(se_uni)));

  // First deal with the --help option. If specified, print help and
  // exit gracefully. Otherwise, call notify and enforce required
  // arguments.
  if (vm.count("help")) {
    // If --help is specified with a heuristic, pass it on to the
    // heuristic 
    if (vm.count("heuristic") == 0) {
      std::cout << usage.str() << std::endl << std::endl;
      std::cout << general_options << std::endl;
      return 0;
    }
  } else if (vm.count("list")) {
    for (auto &kv : heuristic_map) {
      std::cout << "  " << kv.first << std::endl << std::endl;
      std::cout << kv.second->description() << std::endl << std::endl;
    }
    return 0;
  } else {
    // We don't want to call notify if --help is ever specified, since
    // we want to pass --help to the inferior heuristic
    try {
      po::notify(vm);
    } catch (po::required_option &ex) {
      auto name = ex.get_option_name();

      if (name == "--heuristic") {
        std::cerr << "Error: must specify a heuristic" << std::endl;
      } else {
        std::cerr << ex.what() << std::endl;
      }
    
      return 1;
    }
  }



  auto heuristic_name = vm["heuristic"].as<std::string>();

  if (heuristic_map.count(heuristic_name) == 0) {
    std::cerr << "Unknown heuristic: " << heuristic_name << std::endl;
    return 1;
  }

  std::vector<std::string> heuristic_args
    = po::collect_unrecognized(parsed.options, po::include_positional);
  heuristic_args.erase(heuristic_args.begin());
  if (vm.count("help")) {
    heuristic_args.push_back("--help");
  }

  std::vector<std::unique_ptr<Heuristic>> heuristics;
  try {
    heuristics = heuristic_map.at(heuristic_name)->generate(heuristic_args);
  } catch (po::required_option &ex) {
    std::cerr << ex.what() << std::endl;
    return 1;
  } catch (HeuristicFactory::HelpException &ex) {
    return 0;
  } catch (HeuristicFactory::OptionException &ex) {
    std::cerr << ex.what() << std::endl;
    return 1;
  }

  // Read in the toys file
  auto toys_file_string = vm["toys"].as<std::string>();
  std::ifstream toys_file (toys_file_string, std::ifstream::in);

  if (!toys_file.good()) {
    std::cerr << "Error: couldn't read toys file '"
              << toys_file_string << "'" << std::endl;
    return 1;
  }
  auto toys = Toy::load_toys(toys_file);
  toys_file.close();

  Schedule best_schedule;
  Score best_score ("Empty schedule");
  bool once = true;

  std::cerr.precision(std::numeric_limits<double>::digits10);
  std::cout.precision(std::numeric_limits<double>::digits10);
  std::cerr << "Heuristic: score (best score)" << std::endl;
  for (auto &heuristic : heuristics) {
    Schedule schedule = heuristic->solve(toys);
    auto score = schedule.score();

    if (once || (score.feasible and
                 score.objective_value < best_score.objective_value))
      {
        best_schedule = schedule;
        best_score = score;
        once = false;
      }

    std::cerr << heuristic->description() << ": ";

    if (score.feasible) {
      std::cerr << score.objective_value;
    } else {
      std::cerr << "infeasible";
    }

    std::cerr << " (" << best_score.objective_value << ")" << std::endl;
  }

  if (best_score.feasible) {
    std::cerr << "Score: " << best_score.objective_value << std::endl;
    best_schedule.write_solution(std::cout);
  } else {
    std::cerr << "Infeasible solution: " << best_score.message << std::endl;
  }

  return 0;
}

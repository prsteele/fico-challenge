#ifndef SINGLE_ELF_SCHEDULER_FACTORY_HH
#define SINGLE_ELF_SCHEDULER_FACTORY_HH

#include <boost/program_options.hpp>

#include "heuristic_factory.hh"
#include "single_elf_scheduler.hh"
#include "process_medium.hh"

class SingleElfSchedulerFactory: public HeuristicFactory
{
  auto generate(std::vector<std::string> const& heuristic_args)
    -> std::vector<std::unique_ptr<Heuristic>>;

  auto description() -> std::string;
};

#endif

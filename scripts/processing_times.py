"""Creates a plot of job lengths.

"""

import sys
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

def get_processing_times(toys):
    times = []

    with open(toys, 'r') as f:
        once = True
        for line in f:
            if once:
                once = False
                continue

            (toy, arrival, duration) = line.split(',')

            times.append(int(duration))

    return times

def plot_processing_times(times, filename):
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    ax.hist(times, bins=100, log=True)
    ax.set_xlabel('Processing times')
    ax.set_ylabel('log(Count)')
    ax.set_title('The distribution of processing times of toys')

    fig.savefig(filename, bbox_inches='tight', dpi=300)

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('usage: {} toys_file plot.png'.format(sys.argv[0]))
        raise SystemExit(1)

    solution_file = sys.argv[1]
    plot_name = sys.argv[2]

    times = get_processing_times(solution_file)
    plot_processing_times(times, plot_name)

    times.sort()

    print('Min: {}'.format(times[0]))
    print('Max: {}'.format(times[-1]))

    percentiles = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100]

    for p in percentiles:
        x = times[int((len(times) - 1) * (p / 100))]
        print('{}th percentile: {}'.format(p, x))


"""Creates a plot of job lengths plus release dates.

"""

import sys
import matplotlib.pyplot as plt
import datetime

def get_processing_times(toys):
    epoch = datetime.date(2014, 1, 1)
    times = []

    with open(toys, 'r') as f:
        once = True
        for line in f:
            if once:
                once = False
                continue

            (toy, arrival, duration) = line.split(',')

            (year, month, day, hour, minute) = [int(x) for x in arrival.split(' ')]

            date = datetime.date(year, month, day)
            date_minutes = (date - epoch).total_seconds() // 60

            times.append(int(duration) + 60 * hour + minute + date_minutes)

    return times

def plot_processing_times(times, filename):
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    ax.hist(times, bins=100, log=True)
    ax.set_xlabel('Processing times')
    ax.set_ylabel('log(Count)')
    ax.set_title('The distribution of the sum of release dates and processing times of toys')

    fig.savefig(filename, bbox_inches='tight', dpi=300)

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('usage: {} toys_file plot.png'.format(sys.argv[0]))
        raise SystemExit(1)

    solution_file = sys.argv[1]
    plot_name = sys.argv[2]

    times = get_processing_times(solution_file)
    plot_processing_times(times, plot_name)

    _min = min(times)
    _max = max(times)

    print('Min: {}'.format(_min))
    print('Max: {}'.format(_max))


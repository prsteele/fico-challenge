"""This script shows the efficiencies of jobs done by the bottleneck
elf.

"""

import sys
import matplotlib
import matplotlib.cm as cm
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import datetime
import numpy as np

def get_bottleneck_elf_schedule(schedule_file, toys_file):
    """Return a list of (start_time, base processing time, actual
    processing time, efficiency) tuples for the bottleneck elf.

    """

    epoch = datetime.date(2014, 1, 1)

    # Determine how long each toy takes to process at 1.0 efficiency
    processing_times = {}
    with open(toys_file, 'r') as f:
        lines = iter(f)
        next(lines) # Throw away the banner
        
        for line in f:
            (toy, arrival, duration) = line.split(',')

            processing_times[int(toy)] = int(duration)

    print('Done processing toys')

    schedule = []
    with open(schedule_file, 'r') as f:
        lines = list(f)[1:] # Throw away the header

        # Find the bottleneck elf
        completion_times = {}
        for line in lines[::-1]:
            (_toy, _elf, _start, _duration) = line.split(',')
            (toy, elf, duration) = (int(_toy), int(_elf), int(_duration))

            if len(completion_times) == 900:
                break
            
            if elf in completion_times:
                continue
            
            (year, month, day, hour, minute) = [int(x) for x in _start.split(' ')]

            date = datetime.date(year, month, day)
            date_minutes = (date - epoch).total_seconds() // 60

            start = date_minutes + 60 * hour + minute

            completion_times[elf] = start + duration

        bottleneck_elf = max(completion_times, key=lambda x: completion_times[x])

        print('Found the bottleneck elf')

        # Now record the schedule for the bottleneck elf
        for line in lines:
            (_toy, _elf, _start, _duration) = line.split(',')
            (toy, elf, duration) = (int(_toy), int(_elf), int(_duration))

            if elf != bottleneck_elf:
                continue

            (year, month, day, hour, minute) = [int(x) for x in _start.split(' ')]

            date = datetime.date(year, month, day)
            date_minutes = (date - epoch).total_seconds() // 60

            start = date_minutes + 60 * hour + minute

            eff = processing_times[toy] / duration

            schedule.append((start, processing_times[toy], duration, eff))

    print('Done processing the schedule')
    return schedule

def plot_elf(schedule, plot_file):
    """Given a list of (base processing time, actual processing time,
    efficiency) tuples, create a plot of efficiency over time.

    """

    X = []
    Y = []
    C = []
    max_duration = 0
    for (start, base, actual, eff) in schedule:
        X.append(start)
        Y.append(eff)
        max_duration = max(max_duration, actual)

    for (start, base, actual, eff) in schedule:
        C.append(cm.spectral(int(actual / max_duration * (cm.spectral.N - 1))))
        
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    for (x, y, c) in zip(X, Y, C):
        ax.scatter([x], [y], color=c, alpha=.4, marker=',')

    ax.set_title('Efficiency of the bottleneck elf while processing jobs')
    ax.set_ylabel('Efficiency')
    ax.set_xlabel('Time')

    ax.set_xlim((0, max(X)))

    fig.savefig(plot_file, bbox_inches='tight', dpi=300)
        

if __name__ == '__main__':
    if len(sys.argv) != 4:
        print('usage: {} toys_file solution_file plot.png'.format(sys.argv[0]))
        raise SystemExit(1)

    toys_file = sys.argv[1]
    schedule_file = sys.argv[2]
    plot_file = sys.argv[3]
    
    schedule = get_bottleneck_elf_schedule(schedule_file, toys_file)

    plot_elf(schedule, plot_file)

"""Creates a plot of the final completion time of each elf in a schedule.

"""

import sys
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import datetime

def get_finish_times(schedule):
    epoch = datetime.date(2014, 1, 1)
    times = {}
    with open(schedule, 'r') as f:
        once = True

        # Don't bother reading the whole file; just check from the end
        # until we've found each Elf once. The first time we find an
        # Elf is its last job.
        lines = list(f)[1:]
        lines.reverse()
        
        for line in lines:
            if once:
                # Throw away the header
                once = False
                continue

            if len(times) == 900:
                break

            (toy, elf, start, duration) = line.split(',')

            elf_id = int(elf)
            (year, month, day, hour, minute) = [int(x) for x in start.split(' ')]
            date = datetime.date(year, month, day)

            minutes = minute + (date - epoch).total_seconds() // 60
            completion_time = minutes + hour * 60 + int(duration)

            if elf_id not in times:
                times[elf_id] = completion_time

    return times

def plot_finish_times(times, filename):
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    X = sorted(times)
    Y = [times[x] for x in X]

    ax.hist(Y, bins=100)

    ax.set_xlabel('Completion time (minutes)')
    ax.set_ylabel('Count')

    ax.set_title('The distribution of elf completion times')

    fig.savefig(filename, bbox_inches='tight', dpi=300)

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('usage: {} solution_file plot.png'.format(sys.argv[0]))
        raise SystemExit(1)

    solution_file = sys.argv[1]
    plot_name = sys.argv[2]

    times = get_finish_times(solution_file)
    plot_finish_times(times, plot_name)

    _min = min(times.values())
    _max = max(times.values())

    print('Min: {}'.format(_min))
    print('Max: {}'.format(_max))
    print('Diff: {}'.format(_max - _min))

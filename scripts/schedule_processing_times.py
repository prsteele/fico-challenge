"""This script is used to create histograms of the length of jobs in a
given schedule.

"""

import sys
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import datetime

def get_processing_times(schedule):
    epoch = datetime.date(2014, 1, 1)

    times = []

    with open(schedule, 'r') as f:
        lines = iter(f)
        next(lines) # Throw away the banner

        for line in lines:
            (_toy, _elf, _start, _duration) = line.split(',')

            duration = int(_duration)

            times.append(duration)

    return times

def plot_processing_times(times, filename):
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    ax.hist(times, bins=100, log=True)

    ax.set_xlabel('Processing time')
    ax.set_ylabel('log(Count)')
    ax.set_title('The distribution of processing times of toys')

    fig.savefig(filename, bbox_inches='tight', dpi=300)

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('usage: {} solution_file plot.png'.format(sys.argv[0]))
        raise SystemExit(1)

    solution_file = sys.argv[1]
    plot_name = sys.argv[2]

    times = get_processing_times(solution_file)
    plot_processing_times(times, plot_name)
